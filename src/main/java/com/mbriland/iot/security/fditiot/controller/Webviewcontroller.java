package com.mbriland.iot.security.fditiot.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.mbriland.iot.security.fditiot.antlr.impl.Op;
import com.mbriland.iot.security.fditiot.antlr.impl.SelectionCriterion;
import com.mbriland.iot.security.fditiot.entity.ConfigurationRecord;
import com.mbriland.iot.security.fditiot.entity.Record;
import com.mbriland.iot.security.fditiot.service.ConfigurationService;
import com.mbriland.iot.security.fditiot.service.RecordService;

import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

@Controller
public class Webviewcontroller {

	private JSObject javascriptConnector;

	/** for communication from the Javascript engine. */
	private JavaConnector javaConnector = new JavaConnector();;

	@FXML
	WebView webview;

	String scenar;

	@Autowired
	RecordService recordService;
	
	@Autowired
	ConfigurationService configurationService;

	public void initData(String scenar) {
		URL url = this.getClass().getResource("/com/mbriland/iot/security/fditiot/webview/plot.html");

		final WebEngine webEngine = webview.getEngine();

		// set up the listener
		webEngine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
			if (Worker.State.SUCCEEDED == newValue) {
				// set an interface object named 'javaConnector' in the web engine's page
				JSObject window = (JSObject) webEngine.executeScript("window");
				window.setMember("javaConnector", javaConnector);

				// get the Javascript connector object.
				javascriptConnector = (JSObject) webEngine.executeScript("getJsConnector()");
			}
		});

		this.scenar = scenar;
		webEngine.load(url.toString());
		
	}

	public void loadProperty() {

		ArrayList<String> propertyList = new ArrayList<>();
		for (ConfigurationRecord conf : configurationService.getAllPropertyConfiguration(scenar)) {
			propertyList.add(conf.getProperty());
		}
		
		List<String> listThings = recordService.findDistinctThings(scenar);
		
		System.out.println(scenar+" : "+listThings);
		
		javascriptConnector.call("loadData",ArrayUtils.toPrimitive(propertyList.toArray(new String[propertyList.size()])),ArrayUtils.toPrimitive(listThings.toArray(new String[listThings.size()])));
		System.out.println("loaddata");


	}
	
	public void showRecord(String propertyName, String ident) {
		ArrayList<SelectionCriterion> select = new ArrayList<SelectionCriterion>();
		select.add(new SelectionCriterion("meter_code", Op.EQUAL, ident));
		ArrayList<Record> records = (ArrayList<Record>) recordService.query(select, this.scenar);

		ArrayList<Double> x = new ArrayList<>();
		ArrayList<Double> y = new ArrayList<>();

		for (Record record : records) {

			x.add(Double.parseDouble(record.getProperty().get("timestamp").toString()));
			if (record.getProperty().get(propertyName) != null) {
				y.add(Double.parseDouble(record.getProperty().get(propertyName).toString()));

			}

		}

		javascriptConnector.call("sendData", ArrayUtils.toPrimitive(x.toArray(new Double[x.size()])),
				ArrayUtils.toPrimitive(y.toArray(new Double[y.size()])));
	}

	public class JavaConnector {
		/**
		 * called when the JS side wants a String to be converted.
		 *
		 * @param value the String to convert
		 */
		public void loadPropertyJs() {
			System.out.println("loadproperty");
			loadProperty();
		}
		
		public void select(String propertyName, String ident) {
			showRecord(propertyName,ident);
		}
	}

}
