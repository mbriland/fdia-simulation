package com.mbriland.iot.security.fditiot.antlr.impl;

public class TimeFrame {

	private Integer from;
	private Integer to;

	public TimeFrame(Integer from, Integer to) {
		super();
		this.from = from;
		this.to = to;
	}

	public Object getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getTo() {
		return to;
	}

	public void setTo(Integer to) {
		this.to = to;
	}

}
