package com.mbriland.iot.security.fditiot.entity;

import java.util.ArrayList;
import java.util.HashSet;

public class JsonImportedWithProperty {

	ArrayList<Record> record;
	
	HashSet<String> propertyHashSet;

	public JsonImportedWithProperty(ArrayList<Record> record, HashSet<String> propertyHashSet) {
		super();
		this.record = record;
		this.propertyHashSet = propertyHashSet;
	}

	public ArrayList<Record> getRecord() {
		return record;
	}

	public void setRecordMongos(ArrayList<Record> record) {
		this.record = record;
	}

	public HashSet<String> getPropertyHashSet() {
		return propertyHashSet;
	}

	public void setPropertyHashSet(HashSet<String> propertyHashSet) {
		this.propertyHashSet = propertyHashSet;
	}

	
}
