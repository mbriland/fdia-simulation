package com.mbriland.iot.security.fditiot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.stereotype.Service;

import com.mbriland.iot.security.fditiot.entity.ConfigurationRecord;

@Service
public class ConfigurationService {

	@Autowired
	MongoTemplate mongoTemplate;
	
	
	public void insertConfigInCollection(ConfigurationRecord configurationRecord, String collection) {
		mongoTemplate.insert(configurationRecord, collection);
	}
	
	public void insertAllConfigInCollection(List<ConfigurationRecord> list, String collection) {
		BulkOperations bulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, ConfigurationRecord.class, collection);
		bulk.insert(list);
		bulk.execute();
	}
	
	public List<ConfigurationRecord> getAllPropertyConfiguration(String scenario) {
		
		return mongoTemplate.findAll(ConfigurationRecord.class, scenario+"_config");
	}
	
}
