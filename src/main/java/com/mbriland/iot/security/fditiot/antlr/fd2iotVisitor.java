// Generated from fd2iot.g4 by ANTLR 4.8
package com.mbriland.iot.security.fditiot.antlr;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link fd2iotParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface fd2iotVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#root}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRoot(fd2iotParser.RootContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#scenarioDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitScenarioDeclaration(fd2iotParser.ScenarioDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#executionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExecutionList(fd2iotParser.ExecutionListContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#execution}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExecution(fd2iotParser.ExecutionContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#selectionCriteria}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectionCriteria(fd2iotParser.SelectionCriteriaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualSelection(fd2iotParser.EqualSelectionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code greaterThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreaterThanSelection(fd2iotParser.GreaterThanSelectionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lessThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThanSelection(fd2iotParser.LessThanSelectionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code differentSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDifferentSelection(fd2iotParser.DifferentSelectionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code isInsideCircleSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsInsideCircleSelection(fd2iotParser.IsInsideCircleSelectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#timeframe}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeframe(fd2iotParser.TimeframeContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#attenuationCriteria}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttenuationCriteria(fd2iotParser.AttenuationCriteriaContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#alterationCriteria}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlterationCriteria(fd2iotParser.AlterationCriteriaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code affectation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAffectation(fd2iotParser.AffectationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code evolution}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvolution(fd2iotParser.EvolutionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code offset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOffset(fd2iotParser.OffsetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplyOffset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyOffset(fd2iotParser.MultiplyOffsetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code evolutionIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvolutionIncrementation(fd2iotParser.EvolutionIncrementationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayIncrementation(fd2iotParser.ArrayIncrementationContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#evol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvol(fd2iotParser.EvolContext ctx);
	/**
	 * Visit a parse tree produced by {@link fd2iotParser#array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(fd2iotParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentType(fd2iotParser.IdentTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringLiteralType(fd2iotParser.StringLiteralTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code decimalLitealType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimalLitealType(fd2iotParser.DecimalLitealTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code realLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealLiteralType(fd2iotParser.RealLiteralTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code hexaDecimalType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHexaDecimalType(fd2iotParser.HexaDecimalTypeContext ctx);
}