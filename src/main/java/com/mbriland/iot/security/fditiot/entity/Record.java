package com.mbriland.iot.security.fditiot.entity;

import java.util.Map;

import org.springframework.data.annotation.Id;

public class Record {

		@Id
		public String id;
		
		
		private Map<String,Object> property;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Map<String, Object> getProperty() {
			return property;
		}

		public void setProperty( Map<String, Object> property) {
			this.property = property;
		}

		@Override
		public String toString() {
			return "Record [id=" + id + ", property=" + property + "]";
		}

		
	}
