package com.mbriland.iot.security.fditiot.antlr.impl;

import java.util.List;

public class Scenario {

	private String Name;
	private Integer ticker;
	private List<Execution> execs;
	private String geoLocation;

	public Scenario(List<Execution> execs) {
		super();
		this.execs = execs;
	}

	public Scenario() {
		// TODO Auto-generated constructor stub
	}

	public List<Execution> getExecs() {
		return execs;
	}

	public void setExecs(List<Execution> execs) {
		this.execs = execs;
	}

	
	
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Integer getTicker() {
		return ticker;
	}

	public void setTicker(Integer ticker) {
		this.ticker = ticker;
	}

	
	
	public String getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(String geoLocation) {
		this.geoLocation = geoLocation;
	}
	

	@Override
	public String toString() {
		return "Scenario [Name=" + Name + ", ticker=" + ticker + ", execs=" + execs + ", geoLocation=" + geoLocation
				+ "]";
	}
	
	

}
