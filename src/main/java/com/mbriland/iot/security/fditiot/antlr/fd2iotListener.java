// Generated from fd2iot.g4 by ANTLR 4.8
package com.mbriland.iot.security.fditiot.antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link fd2iotParser}.
 */
public interface fd2iotListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#root}.
	 * @param ctx the parse tree
	 */
	void enterRoot(fd2iotParser.RootContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#root}.
	 * @param ctx the parse tree
	 */
	void exitRoot(fd2iotParser.RootContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#scenarioDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterScenarioDeclaration(fd2iotParser.ScenarioDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#scenarioDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitScenarioDeclaration(fd2iotParser.ScenarioDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#executionList}.
	 * @param ctx the parse tree
	 */
	void enterExecutionList(fd2iotParser.ExecutionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#executionList}.
	 * @param ctx the parse tree
	 */
	void exitExecutionList(fd2iotParser.ExecutionListContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#execution}.
	 * @param ctx the parse tree
	 */
	void enterExecution(fd2iotParser.ExecutionContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#execution}.
	 * @param ctx the parse tree
	 */
	void exitExecution(fd2iotParser.ExecutionContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#selectionCriteria}.
	 * @param ctx the parse tree
	 */
	void enterSelectionCriteria(fd2iotParser.SelectionCriteriaContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#selectionCriteria}.
	 * @param ctx the parse tree
	 */
	void exitSelectionCriteria(fd2iotParser.SelectionCriteriaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void enterEqualSelection(fd2iotParser.EqualSelectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void exitEqualSelection(fd2iotParser.EqualSelectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code greaterThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void enterGreaterThanSelection(fd2iotParser.GreaterThanSelectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code greaterThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void exitGreaterThanSelection(fd2iotParser.GreaterThanSelectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lessThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void enterLessThanSelection(fd2iotParser.LessThanSelectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lessThanSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void exitLessThanSelection(fd2iotParser.LessThanSelectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code differentSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void enterDifferentSelection(fd2iotParser.DifferentSelectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code differentSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void exitDifferentSelection(fd2iotParser.DifferentSelectionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code isInsideCircleSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void enterIsInsideCircleSelection(fd2iotParser.IsInsideCircleSelectionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code isInsideCircleSelection}
	 * labeled alternative in {@link fd2iotParser#selectionCriterion}.
	 * @param ctx the parse tree
	 */
	void exitIsInsideCircleSelection(fd2iotParser.IsInsideCircleSelectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#timeframe}.
	 * @param ctx the parse tree
	 */
	void enterTimeframe(fd2iotParser.TimeframeContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#timeframe}.
	 * @param ctx the parse tree
	 */
	void exitTimeframe(fd2iotParser.TimeframeContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#attenuationCriteria}.
	 * @param ctx the parse tree
	 */
	void enterAttenuationCriteria(fd2iotParser.AttenuationCriteriaContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#attenuationCriteria}.
	 * @param ctx the parse tree
	 */
	void exitAttenuationCriteria(fd2iotParser.AttenuationCriteriaContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#alterationCriteria}.
	 * @param ctx the parse tree
	 */
	void enterAlterationCriteria(fd2iotParser.AlterationCriteriaContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#alterationCriteria}.
	 * @param ctx the parse tree
	 */
	void exitAlterationCriteria(fd2iotParser.AlterationCriteriaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code affectation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterAffectation(fd2iotParser.AffectationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code affectation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitAffectation(fd2iotParser.AffectationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code evolution}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterEvolution(fd2iotParser.EvolutionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code evolution}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitEvolution(fd2iotParser.EvolutionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code offset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterOffset(fd2iotParser.OffsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code offset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitOffset(fd2iotParser.OffsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyOffset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyOffset(fd2iotParser.MultiplyOffsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyOffset}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyOffset(fd2iotParser.MultiplyOffsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code evolutionIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterEvolutionIncrementation(fd2iotParser.EvolutionIncrementationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code evolutionIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitEvolutionIncrementation(fd2iotParser.EvolutionIncrementationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void enterArrayIncrementation(fd2iotParser.ArrayIncrementationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayIncrementation}
	 * labeled alternative in {@link fd2iotParser#alterationCriterion}.
	 * @param ctx the parse tree
	 */
	void exitArrayIncrementation(fd2iotParser.ArrayIncrementationContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#evol}.
	 * @param ctx the parse tree
	 */
	void enterEvol(fd2iotParser.EvolContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#evol}.
	 * @param ctx the parse tree
	 */
	void exitEvol(fd2iotParser.EvolContext ctx);
	/**
	 * Enter a parse tree produced by {@link fd2iotParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArray(fd2iotParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link fd2iotParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArray(fd2iotParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void enterIdentType(fd2iotParser.IdentTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void exitIdentType(fd2iotParser.IdentTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void enterStringLiteralType(fd2iotParser.StringLiteralTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void exitStringLiteralType(fd2iotParser.StringLiteralTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code decimalLitealType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void enterDecimalLitealType(fd2iotParser.DecimalLitealTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code decimalLitealType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void exitDecimalLitealType(fd2iotParser.DecimalLitealTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code realLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void enterRealLiteralType(fd2iotParser.RealLiteralTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code realLiteralType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void exitRealLiteralType(fd2iotParser.RealLiteralTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code hexaDecimalType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void enterHexaDecimalType(fd2iotParser.HexaDecimalTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code hexaDecimalType}
	 * labeled alternative in {@link fd2iotParser#type}.
	 * @param ctx the parse tree
	 */
	void exitHexaDecimalType(fd2iotParser.HexaDecimalTypeContext ctx);
}