package com.mbriland.iot.security.fditiot;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import com.mbriland.iot.security.fditiot.config.StageManager;
import com.mbriland.iot.security.fditiot.view.FxmlView;

@SpringBootApplication
public class Main extends Application {
	private static final Logger logger = LoggerFactory.getLogger(Main.class);	

    protected ConfigurableApplicationContext springContext;
    protected StageManager stageManager;

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        springContext = springBootApplicationContext();
    }

    @Override
    public void start(Stage stage) throws Exception {
    	logger.info("APP STARTING");
        stageManager = springContext.getBean(StageManager.class, stage);
        displayInitialScene();

    }

    @Override
    public void stop() throws Exception {
        springContext.close();
    	Platform.exit();
    	System.out.println("exiting");
    	System.exit(0);
    }

    /**
     * Useful to override this method by sub-classes wishing to change the first
     * Scene to be displayed on startup. Example: Functional tests on main
     * window.
     */
    protected void displayInitialScene() {
        stageManager.switchScene(FxmlView.ROOT);
    }

    
    private ConfigurableApplicationContext springBootApplicationContext() {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Main.class);
        String[] args = getParameters().getRaw().stream().toArray(String[]::new);
        return builder.run(args);
    }

}
