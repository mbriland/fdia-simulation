package com.mbriland.iot.security.fditiot.controller;

import java.util.ArrayList;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mbriland.iot.security.fditiot.antlr.impl.Op;
import com.mbriland.iot.security.fditiot.antlr.impl.SelectionCriterion;
import com.mbriland.iot.security.fditiot.entity.ConfigurationRecord;
import com.mbriland.iot.security.fditiot.entity.Record;
import com.mbriland.iot.security.fditiot.service.ConfigurationService;
import com.mbriland.iot.security.fditiot.service.RecordService;

import de.gsi.chart.XYChart;
import de.gsi.chart.axes.spi.DefaultNumericAxis;
import de.gsi.chart.plugins.EditAxis;
import de.gsi.chart.plugins.Zoomer;
import de.gsi.dataset.spi.DataSetBuilder;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;

@Component
public class ChartController {

	@FXML
	BorderPane borderPane;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	RecordService recordService;

	@Autowired
	ConfigurationService configurationService;

	@FXML
	ChoiceBox<String> propertyPicker;

	@FXML
	Button select;

	private String scenar;

	private XYChart chart;
	private DefaultNumericAxis xAxis1;
	private DefaultNumericAxis yAxis1;

	public void initData(String scenar) {
		this.scenar = scenar;

		// ArrayList<Record> records = (ArrayList<Record>)
		// mongoTemplate.findAll(Record.class, scenar);

		ObservableList<String> items = FXCollections.observableArrayList();
		for (ConfigurationRecord conf : configurationService.getAllPropertyConfiguration(scenar)) {
			items.add(conf.getProperty());
		}
		propertyPicker.setItems(items);

		/*
		 * ArrayList<SelectionCriterion> select = new ArrayList<SelectionCriterion>();
		 * select.add(new SelectionCriterion("meter_code", Op.EQUAL, "10"));
		 * ArrayList<Record> records = (ArrayList<Record>) recordService.query(select,
		 * this.scenar);
		 * 
		 * ArrayList<Double> x = new ArrayList<>(); ArrayList<Double> y = new
		 * ArrayList<>();
		 * 
		 * for (Record record : records) {
		 * 
		 * x.add(Double.parseDouble(record.getProperty().get("timestamp").toString()));
		 * y.add(Double.parseDouble(record.getProperty().get("data*humidityRH").toString
		 * ()));
		 * 
		 * }
		 */

		xAxis1 = new DefaultNumericAxis("timestamp");
		xAxis1.setAnimated(false);
		yAxis1 = new DefaultNumericAxis("select data");
		yAxis1.setAnimated(false);
		// yAxis2.setSide(Side.LEFT); // unusual but possible case

		chart = new XYChart(xAxis1, yAxis1);

		final Zoomer zoom = new Zoomer();
		// add axes that shall be excluded from the zoom action
		// alternatively (uncomment):
		// Zoomer.setOmitZoom(yAxis3, true);
		chart.getPlugins().add(zoom);

		chart.getPlugins().add(new EditAxis());

		/*
		 * chart.getRenderers().get(0).getDatasets() .setAll(new
		 * DataSetBuilder().setXValues(ArrayUtils.toPrimitive(x.toArray(new
		 * Double[x.size()]))) .setYValues(ArrayUtils.toPrimitive(y.toArray(new
		 * Double[y.size()]))).build());
		 */

		borderPane.setCenter(chart);

	}

	public void onClickSelect() {

		ArrayList<SelectionCriterion> select = new ArrayList<SelectionCriterion>();
		select.add(new SelectionCriterion("meter_code", Op.EQUAL, "10"));
		ArrayList<Record> records = (ArrayList<Record>) recordService.query(select, this.scenar);

		ArrayList<Double> x = new ArrayList<>();
		ArrayList<Double> y = new ArrayList<>();

		for (Record record : records) {

			x.add(Double.parseDouble(record.getProperty().get("timestamp").toString()));
			if (record.getProperty().get(propertyPicker.getSelectionModel().getSelectedItem()) != null) {
				y.add(Double.parseDouble(
						record.getProperty().get(propertyPicker.getSelectionModel().getSelectedItem()).toString()));

			}

		}

		yAxis1.setLabel(propertyPicker.getSelectionModel().getSelectedItem());

		if (x.isEmpty() || y.isEmpty()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Error in the chart");
			alert.setContentText("The chart is empty");
			alert.showAndWait();
		} else {
			chart.getRenderers().get(0).getDatasets()
					.setAll(new DataSetBuilder().setXValues(ArrayUtils.toPrimitive(x.toArray(new Double[x.size()])))
							.setYValues(ArrayUtils.toPrimitive(y.toArray(new Double[y.size()]))).build());
			}

	}


	@FXML
	public void exitApplication(ActionEvent event) {
		Platform.exit();
	}
}
