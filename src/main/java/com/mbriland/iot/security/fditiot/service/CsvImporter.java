package com.mbriland.iot.security.fditiot.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import com.mbriland.iot.security.fditiot.entity.DataType;
import com.mbriland.iot.security.fditiot.entity.JsonImportedWithProperty;
import com.mbriland.iot.security.fditiot.entity.Record;

@Service
public class CsvImporter {

	
	public JsonImportedWithProperty importCsv(File selectedFile) throws IOException {

		JsonImportedWithProperty imported= null;
		
		Reader in = new FileReader(selectedFile.getAbsolutePath());
		
		imported = listRecord(in);
		
		
		return imported;

	}

	private JsonImportedWithProperty listRecord(Reader in) throws IOException {
		
		final List<Record> resultat = new ArrayList<Record>();
		
		Record record;
		Map<String, Object> propertys;

		
        CSVFormat csvFormat = CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader().withQuote(null).withNullString("");
        CSVParser csvParser = csvFormat.parse(in);
        
        
        Map<String, Integer> headers = csvParser.getHeaderMap();
        HashSet<String> propertyHashset = new HashSet<>();
        

        for (Map.Entry<String, Integer> header : headers.entrySet()) {    	
			propertyHashset.add(header.getKey());
		}

        Iterable<CSVRecord> records = csvParser;
        
		for (CSVRecord csvRecord : records) {
			record = new Record();
			propertys = new HashMap<String, Object>();
			for (String header : propertyHashset) {
				if(csvRecord.get(header) != null) {
					//System.out.println(header + " " +csvRecord.get(header));
					propertys.put(header, DataType.getType(csvRecord.get(header)).getValue(csvRecord.get(header)));
				}
			}
			
			record.setProperty(propertys);
			resultat.add(record);
		}
		
		
		JsonImportedWithProperty js = new JsonImportedWithProperty((ArrayList<Record>) resultat, propertyHashset);
		
		return js;
	}
	
}
