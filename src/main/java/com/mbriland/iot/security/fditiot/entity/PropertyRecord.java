package com.mbriland.iot.security.fditiot.entity;

public class PropertyRecord {

	public String name;
	public Object value;

	public PropertyRecord(String name, Object object) {
		super();
		this.name = name;
		this.value = object;
	}

	public PropertyRecord() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
