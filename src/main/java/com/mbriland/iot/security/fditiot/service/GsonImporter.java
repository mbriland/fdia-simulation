package com.mbriland.iot.security.fditiot.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.mbriland.iot.security.fditiot.entity.JsonImportedWithProperty;
import com.mbriland.iot.security.fditiot.entity.Record;

@Service
public class GsonImporter {

	final private HashSet<String> propertyHashSet = new HashSet<String>();

	public JsonImportedWithProperty importJson(File selectedFile) throws IOException {

		List<Record> listRecord = null;

		JsonReader reader = new JsonReader(new InputStreamReader(new FileInputStream(selectedFile)));
				//new StringReader(GsonImporter.readLineByLineJava8(selectedFile.getAbsolutePath())));
		listRecord = listRecord(reader);
		
		JsonImportedWithProperty js = new JsonImportedWithProperty((ArrayList<Record>) listRecord, propertyHashSet);
		return js;

	}

	private List<Record> listRecord(final JsonReader reader) throws IOException {
		final List<Record> resultat = new ArrayList<Record>();
		reader.beginArray();
		int i=0;
		while (reader.hasNext()) {
			resultat.add(lireRecord(reader));
			System.out.println(i++);

		}

		return resultat;
	}

	private Record lireRecord(final JsonReader reader) throws IOException {

		final Record record = new Record();
		final Map<String, Object> propertys = new HashMap<String, Object>();
		reader.beginObject();
		while (reader.hasNext()) {
			final String name = reader.nextName();
			propertyHashSet.add(name.replaceAll("\\.", "*"));
			if (reader.peek().equals(JsonToken.NUMBER)) {
				propertys.put(name, reader.nextDouble());
			} else if (reader.peek().equals(JsonToken.STRING)) {
				propertys.put(name, reader.nextString());
			} else if (reader.peek().equals(JsonToken.BOOLEAN)) {
				propertys.put(name, reader.nextBoolean());
			} else if (reader.peek().equals(JsonToken.NULL)) {
				propertys.put(name, null);
				reader.skipValue();
			} else {
				reader.skipValue();
			}
		}
		record.setProperty(propertys);
		reader.endObject();
		return record;
	}

	private static String readLineByLineJava8(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}

}
