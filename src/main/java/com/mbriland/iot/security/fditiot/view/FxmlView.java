package com.mbriland.iot.security.fditiot.view;

import java.util.ResourceBundle;

public enum FxmlView {

	CHART {
		@Override
		public String getTitle() {
			return getStringFromResourceBundle("chart.title");
		}

		@Override
		public String getFxmlFile() {
			return "/fxml/Chart.fxml";
		}
	},
	MAPPINGJSON {

		@Override
		public String getTitle() {
			return getStringFromResourceBundle("mappingjson.title");
		}

		@Override
		public String getFxmlFile() {
			return "/fxml/MappingJson.fxml";
		}

	},
	ROOT {
		@Override
		public String getTitle() {
			return getStringFromResourceBundle("root.title");
		}

		@Override
		public String getFxmlFile() {
			return "/fxml/RootLayout.fxml";
		}
	},WEBVIEW {
		@Override
		public String getTitle() {
			return getStringFromResourceBundle("root.title");
		}

		@Override
		public String getFxmlFile() {
			return "/fxml/webviewchart.fxml";
		}
	};


	public abstract String getTitle();

	public abstract String getFxmlFile();

	String getStringFromResourceBundle(String key) {
		return ResourceBundle.getBundle("Bundle").getString(key);
	}

}
