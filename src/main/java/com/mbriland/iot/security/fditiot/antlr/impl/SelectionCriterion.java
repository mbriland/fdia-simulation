package com.mbriland.iot.security.fditiot.antlr.impl;

import java.util.List;

public class SelectionCriterion {
	private String ident;
	private Op op;
	private Object expr;
	

	public SelectionCriterion(String ident, Op op, Object expr) {
		super();
		this.ident = ident;
		this.op = op;
		this.expr = expr;
	}

	public SelectionCriterion() {
		// TODO Auto-generated constructor stub
	}

	public String getIdent() {
		return ident;
	}

	public void setIdent(String ident) {
		this.ident = ident;
	}

	public Op getOp() {
		return op;
	}

	public void setOp(Op op) {
		this.op = op;
	}

	public Object getExpr() {
		return expr;
	}

	public void setExpr(Object expr) {
		this.expr = expr;
	}

	@Override
	public String toString() {
		return "SelectionCriterion [ident=" + ident + ", op=" + op + ", expr=" + expr + "]";
	}

	
	

}
