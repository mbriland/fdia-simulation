package com.mbriland.iot.security.fditiot.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Stream;

import org.json.*;
import org.springframework.stereotype.Controller;

import com.mbriland.iot.security.fditiot.entity.JsonImportedWithProperty;
import com.mbriland.iot.security.fditiot.entity.PropertyRecord;
import com.mbriland.iot.security.fditiot.entity.Record;
import com.mbriland.iot.security.fditiot.entity.RecordMongo;

@Controller
public class JsonImporter {


	HashSet<String> propertyHashSet = new HashSet<String>();

	public JsonImportedWithProperty importJson(File selectedFile) throws FileNotFoundException {

		Record record = new Record();
		ArrayList<Record> records = new ArrayList<Record>();
		String fileString = JsonImporter.readLineByLineJava8(selectedFile.getAbsolutePath());

		JSONArray jsonArray = new JSONArray(fileString);
		fileString = null;
	
			for (Object object : jsonArray) {
				//String jsonflattened = new JsonFlattener(object.toString()).withFlattenMode(FlattenMode.MONGODB)
					//	.withSeparator('*').withPrintMode(PrintMode.PRETTY).flatten();
				record = generateRecord2(object.toString());
				//jsonflattened=null;
				records.add(record);
			}
		
		System.out.println(records.size() + "TAILLE");
		JsonImportedWithProperty recordImportedWithProperty = new JsonImportedWithProperty(records, propertyHashSet);
		return recordImportedWithProperty;
	}

	// TODO rename mathod
	public RecordMongo generateRecord(String jsonflattened) {

		RecordMongo recordMongo = new RecordMongo();
		ArrayList<PropertyRecord> propertyRecords = new ArrayList<PropertyRecord>();

		JSONObject record = new JSONObject(jsonflattened);
		Iterator<String> keysIterator = record.keys();
		while (keysIterator.hasNext()) {
			String key = keysIterator.next();

			propertyRecords.add(new PropertyRecord(key, record.get(key)));
			propertyHashSet.add(key);
		}
		recordMongo.setPropertyRecords(propertyRecords);

		return recordMongo;
	}

	public Record generateRecord2(String jsonflattened) {

		Record record = new Record();

		HashMap<String, Object> mapProperty = new HashMap<>();

		JSONObject json = new JSONObject(jsonflattened);
		Iterator<String> keysIterator = json.keys();
		while (keysIterator.hasNext()) {
			String key = keysIterator.next();

			mapProperty.put(key, json.get(key));
			propertyHashSet.add(key);
		}
		record.setProperty(mapProperty);
		json=null;
		return record;
	}

	private static String readLineByLineJava8(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}

}
