package com.mbriland.iot.security.fditiot.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.OutOperation;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.EvolutionIncrementationContext;
import com.mbriland.iot.security.fditiot.antlr.impl.AlterationCriterion;
import com.mbriland.iot.security.fditiot.antlr.impl.AlterationCriterionArray;
import com.mbriland.iot.security.fditiot.antlr.impl.EvolutionCriterion;
import com.mbriland.iot.security.fditiot.antlr.impl.Execution;
import com.mbriland.iot.security.fditiot.antlr.impl.Op;
import com.mbriland.iot.security.fditiot.antlr.impl.Scenario;
import com.mbriland.iot.security.fditiot.antlr.impl.SelectionCriterion;
import com.mbriland.iot.security.fditiot.entity.Record;
import com.mongodb.BasicDBObject;

@Service
public class ScenarioService {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	RecordService recordService;

	public void execute(Scenario scenario, String collection) {

		// CREATION DE LA NOUVELLE COLLECTION PAR COPIE
		OutOperation outOperation = new OutOperation(collection + "_" + scenario.getName());
		mongoTemplate.aggregate(Aggregation.newAggregation(outOperation), collection, BasicDBObject.class);

		// Creation de la nouvelle confiuration par copie
		outOperation = new OutOperation(collection + "_" + scenario.getName() + "_config");
		mongoTemplate.aggregate(Aggregation.newAggregation(outOperation), collection + "_config", BasicDBObject.class);

		for (Execution exec : scenario.getExecs()) {

			switch (exec.getPrimitive()) {
			case CREATE:
				System.out.println("CREATE!!!!!!!!");
				this.create(exec, collection, scenario.getTicker(), scenario.getName());
				break;
			case ALTER:
				System.out.println("ALTER!!!!!");
				this.alter(exec, collection, scenario.getTicker(), scenario.getName(), scenario.getGeoLocation());
				break;
			case COPY:
				this.copy(exec, collection, scenario.getTicker(), scenario.getName(), scenario.getGeoLocation());
				break;
			case DELETE:
				this.delete(exec, collection, scenario.getTicker(), scenario.getName());
				break;

			default:
				break;
			}
		}

		return;
	}

	private void delete(Execution exec, String collection, Integer integer, String name) {

		System.out.println("delete");
		List<SelectionCriterion> selectcrit = exec.getSelectionCriterion();
		selectcrit.add(new SelectionCriterion("timestamp", Op.GREATERTHAN, exec.getTimeframe().getFrom()));
		selectcrit.add(new SelectionCriterion("timestamp", Op.LESSERTHAN, exec.getTimeframe().getTo()));
		Query query = recordService.querySelectionBuilder(selectcrit);

		recordService.delete(query, collection + '_' + name);
	}

	private void copy(Execution exec, String collection, Integer ticker, String name,String geolocation) {

		
		List<SelectionCriterion> selectcrit = exec.getSelectionCriterion(); // liste des criteres de selection
		selectcrit.add(new SelectionCriterion("timestamp", Op.GREATERTHAN, exec.getTimeframe().getFrom()));
		selectcrit.add(new SelectionCriterion("timestamp", Op.LESSERTHAN, exec.getTimeframe().getTo()));
		
		Query query = recordService.querySelectionBuilder(selectcrit);
		
		List<Record> listRecord = recordService.mongoTemplate.find(query, Record.class, collection+'_'+name);
		List<Record> newListRecord = new ArrayList<Record>();
		
		for (int i = 0; i < listRecord.size(); i++) {
			Record record = listRecord.get(i);
			Record newRecord = new Record();
			newRecord.setProperty(record.getProperty());
			newRecord.getProperty().put("fdiotStatus", "undercopy");
			newListRecord.add(newRecord);
			System.out.println(newRecord.toString());

		}
		
	    recordService.insertAll(newListRecord, collection+'_'+name);
	    

	    exec.getSelectionCriterion().add(new SelectionCriterion("fdiotStatus", Op.EQUAL, "undercopy"));
	    alter(exec, collection, ticker, name, geolocation);
	    
	    recordService.unsetProperty("property.fdiotStatus",new SelectionCriterion("property.fdiotStatus",Op.EQUAL,"undercopy"),collection+'_'+name);
	}

	private void alter(Execution exec, String collection, Integer ticker, String name, String geolocation) {

		List<SelectionCriterion> selectcrit = exec.getSelectionCriterion(); // liste des criteres de selection
		selectcrit.add(new SelectionCriterion("timestamp", Op.GREATERTHAN, exec.getTimeframe().getFrom()));
		selectcrit.add(new SelectionCriterion("timestamp", Op.LESSERTHAN, exec.getTimeframe().getTo()));

		Query query = recordService.querySelectionBuilder(selectcrit);

		Update update = new Update();
		for (AlterationCriterion alteration : exec.getAlterationCriterion()) {

			if (alteration.getExpr() instanceof EvolutionCriterion) {

				if (alteration.getOp().equals(Op.INCREMENT)) {
					System.out.println("evolution incremental");
					EvolutionCriterion evol = (EvolutionCriterion) alteration.getExpr();
					if (alteration.getAttenuationCriterion() != null) {
						recordService.incrementalEvolution(query, collection + "_" + name, geolocation,
								alteration.getIdent().toString(), evol.getPas(), evol.getFrom(),
								alteration.getAttenuationCriterion());
					} else {

						recordService.incrementalEvolution(query, collection + "_" + name, geolocation,
								alteration.getIdent().toString(), evol.getPas(), evol.getFrom(), null);
					}
				}

			} else if (alteration.getOp().equals(Op.INCREMENT)) {
				System.out.println("OFFSET/increment");
				if(alteration instanceof AlterationCriterionArray) {
					System.out.println("Alter incrementation d'un tableau");
					AlterationCriterionArray alterationArray = (AlterationCriterionArray)alteration;
					for (int i = alterationArray.getStart(); i < alterationArray.getEnd(); i++) {
						update.inc("property." + alteration.getIdent().toString()+i,
								(Number) Double.parseDouble(alteration.getExpr().toString()));
					}
					
				}else {
					System.out.println("basic increment");
					update.inc("property." + alteration.getIdent().toString(),
							(Number) Double.parseDouble(alteration.getExpr().toString()));
				}
				
			} else if (alteration.getOp().equals(Op.MULTIPLY)) {
				System.out.println("MULTIPLYOFFSET");
				update.multiply("property." + alteration.getIdent().toString(),
						(Number) Double.parseDouble(alteration.getExpr().toString()));
			} else {
				System.out.println("affectation");
				update.set("property." + alteration.getIdent().toString(), alteration.getExpr());
			}

		}

		System.out.println(update.toString());

		recordService.updateThingsInCollection(query, update, collection + "_" + name);
	}

	// Fonction appliquant la primitive d'alteration create
	// TODO Gerer la primitive d'evolution quand le ticker est hors borne
	private void create(Execution exec, String collection, int tiker, String scenarioName) {

		ArrayList<Record> records = new ArrayList<>();
		HashMap<String, Double> memory = new HashMap<>();

		for (int i = (int) exec.getTimeframe().getFrom(); i < (int) exec.getTimeframe().getTo(); i += tiker) {
			Record record = new Record();

			HashMap<String, Object> mapProperty = new HashMap<String, Object>();
			for (AlterationCriterion alte : exec.getAlterationCriterion()) {

				if (alte.getExpr() instanceof EvolutionCriterion) {
					// c'est une evolution a travers le temps
					EvolutionCriterion evol = (EvolutionCriterion) alte.getExpr();
					Double from = evol.getFrom();
					Double to = evol.getTo();
					Double pas = evol.getPas();

					if (memory.get(alte.getIdent().toString()) != null) {
						Double value = memory.get(alte.getIdent().toString());
						mapProperty.put(alte.getIdent().toString(), String.valueOf((value + pas)));
						memory.put(alte.getIdent().toString(), value + pas);
					} else {
						memory.put(alte.getIdent().toString(), from);
						mapProperty.put(alte.getIdent().toString(), from.toString());
					}

				} else {
					// c'est une affectation simple
					mapProperty.put(alte.getIdent().toString(), alte.getExpr().toString());
				}
			}

			record.setProperty(mapProperty);

			records.add(record);
		}
		recordService.insertAll(records, collection + "_" + scenarioName);
	}

}
