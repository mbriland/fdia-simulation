package com.mbriland.iot.security.fditiot.controller;

import com.mbriland.iot.security.fditiot.service.CsvImporter;
import com.mbriland.iot.security.fditiot.service.ExporterService;
import com.mbriland.iot.security.fditiot.service.GsonImporter;
import com.mbriland.iot.security.fditiot.service.JsonImporter;
import com.mbriland.iot.security.fditiot.service.MongoService;
import com.mbriland.iot.security.fditiot.service.RecordService;
import com.mbriland.iot.security.fditiot.service.ScenarioService;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.web.WebView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.collections4.IterableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import com.mbriland.iot.security.fditiot.antlr.fd2iotLexer;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser;
import com.mbriland.iot.security.fditiot.antlr.impl.Circle;
import com.mbriland.iot.security.fditiot.antlr.impl.DescriptiveErrorListener;
import com.mbriland.iot.security.fditiot.antlr.impl.ParseVisitor;
import com.mbriland.iot.security.fditiot.antlr.impl.Scenario;
import com.mbriland.iot.security.fditiot.config.StageManager;
import com.mbriland.iot.security.fditiot.entity.Record;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

@Controller
public class RootLayoutController implements Initializable {

	private static final Logger logger = LoggerFactory.getLogger(RootLayoutController.class);

	@Lazy
	@Autowired
	private StageManager stageManager;

	@Autowired
	private MongoService mongoService;
	
	@Autowired
	private RecordService recordService;

	@Autowired
	ScenarioService scenarioService;

	@Autowired
	JsonImporter jsonImporter;

	@Autowired
	GsonImporter gsonImporter;
	
	@Autowired
	CsvImporter csvImporter;
	
	@Autowired
	ExporterService exporterService;

	@FXML
	TreeView<String> treeScenario;

	@FXML
	Slider sliderTime;

	@FXML
	TextArea textDsl;

	@FXML
	LineChart<String, Double> chartTest;
	
	@FXML
	WebView webview;

	public void onClickOpenJson(ActionEvent actionEvent) throws IOException {
		logger.debug("Importe JSON file");
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);

		if (selectedFile != null) {
			logger.info("File selected: " + selectedFile.getName());
			try {
				// jsonImportedWithProperty = jsonImporter.importJson(selectedFile);

				stageManager.goToMappingJson(gsonImporter.importJson(selectedFile));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("File selection cancelled.");
		}
	}
	
	
	public void onClickOpenCsv(ActionEvent actionEvent) throws IOException {
		logger.debug("Importe CSV file");
		FileChooser fileChooser = new FileChooser();
		File selectedFile = fileChooser.showOpenDialog(null);

		if (selectedFile != null) {
			logger.info("File selected: " + selectedFile.getName());
			try {
				stageManager.goToMappingJson(csvImporter.importCsv(selectedFile));
				//csvImporter.importCsv(selectedFile);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("File selection cancelled.");
		}
	}
	

	public void dropScenario(ActionEvent actionEvent) {
		String selectedScenario = treeScenario.getSelectionModel().getSelectedItem().getValue();
		mongoService.dropCollection(selectedScenario);
		mongoService.dropCollection(selectedScenario + "_config");
		this.updateScenarioList();
	}

	public void onClickReload(ActionEvent actionEvent) {
		logger.debug("Reload scenario");
		this.updateScenarioList();
	}
	
	public void onClickExport() {
		exporterService.export(treeScenario.getSelectionModel().getSelectedItem().getValue());
	}

	public void onClickDsl() throws IOException {
		/*
		 * XYChart.Series<String, Double> series = new XYChart.Series<>();
		 * 
		 * for(int i = 0;i<50;i++) { series.getData().add(new
		 * XYChart.Data<>(i+"",Math.random()*(50-0)));
		 * 
		 * } chartTest.getData().add(series);
		 */
		 
		
		if (treeScenario.getSelectionModel().getSelectedItem() == null) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur selection scenario");
			alert.showAndWait();
			logger.error("Aucun scénario de selectionné");
		} else if (textDsl.getText().isEmpty()) {
 
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur DSL scenario vide");
			alert.showAndWait();
			logger.error("Aucun scénario DSL en entrée");

		} else {

			parseDsl();

		}

	}

	public void parseDsl() {
		logger.debug("Début de l'execution du scenario");
		CharStream charstream = CharStreams.fromString(textDsl.getText());

		try {
			// create a lexer that feeds off of input CharStream
			fd2iotLexer lexer = new fd2iotLexer(charstream);
			lexer.removeErrorListeners();
			lexer.addErrorListener(DescriptiveErrorListener.INSTANCE);
			// create a buffer of tokens pulled from the lexer
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			// create a parser that feeds off the tokens buffer
			fd2iotParser parser = new fd2iotParser(tokens);
			parser.removeErrorListeners();
			parser.addErrorListener(DescriptiveErrorListener.INSTANCE);
			ParseTree tree = parser.root(); // begin parsing at init rule
			System.out.println(tree.toStringTree(parser)); // print LISP-style tree
			ParseVisitor parse = new ParseVisitor();
			Scenario scenario = parse.visit(tree);
			scenario.toString();
			scenarioService.execute(scenario, treeScenario.getSelectionModel().getSelectedItem().getValue());
		} catch (ParseCancellationException e) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur dans le DSL");
			alert.setContentText(e.getMessage());
			alert.showAndWait();

		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Erreur");
			alert.setHeaderText("Erreur !!!");
			alert.setContentText(e.getMessage());
			alert.showAndWait();
			e.printStackTrace();
			logger.error("Un probléme est survenue lors de l'execution du scenario " + e.getMessage());
		}

		this.updateScenarioList();
	}

	public void updateScenarioList() {
		TreeItem<String> rootItem = new TreeItem<String>("Scenario");
		rootItem.setExpanded(true);
		Set<String> collectionSet = mongoService.getCollectionNames();
		TreeItem<String> item;
		if (!IterableUtils.isEmpty(collectionSet)) {
			for (String collectionName : collectionSet) {
				if (!collectionName.contains("_config")) {
					item = new TreeItem<String>(collectionName);
					rootItem.getChildren().add(item);
				}
			}
		}

		treeScenario.setRoot(rootItem);
		treeScenario.expandedItemCountProperty();
	}

	public void onClickChartExploration() {

		stageManager.goToChart(treeScenario.getSelectionModel().getSelectedItem().getValue());
	}
	
	public void onClickwebviewitem() {

		stageManager.goToWebView(treeScenario.getSelectionModel().getSelectedItem().getValue());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		TreeItem<String> rootItem = new TreeItem<String>("Scenario");
		rootItem.setExpanded(true);
		Set<String> collectionSet = mongoService.getCollectionNames();
		TreeItem<String> item;
		if (!IterableUtils.isEmpty(collectionSet)) {
			for (String collectionName : collectionSet) {
				if (!collectionName.contains("_config")) {
					item = new TreeItem<String>(collectionName);
					rootItem.getChildren().add(item);
				}
			}
		}

		treeScenario.setRoot(rootItem);

	}

	@FXML
	public void onCloseRequest(ActionEvent event) {
		Platform.exit();
	}

}
