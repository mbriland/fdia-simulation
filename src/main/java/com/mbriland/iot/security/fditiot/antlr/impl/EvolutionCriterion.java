package com.mbriland.iot.security.fditiot.antlr.impl;

public class EvolutionCriterion {

	private double from;
	private double to;
	private double pas;

	public EvolutionCriterion() {
		super();
	}

	public EvolutionCriterion(double from, double to, double pas) {
		super();
		this.from = from;
		this.to = to;
		this.pas = pas;
	}

	public double getFrom() {
		return from;
	}

	public void setFrom(double from) {
		this.from = from;
	}

	public double getTo() {
		return to;
	}

	public void setTo(double to) {
		this.to = to;
	}

	public double getPas() {
		return pas;
	}

	public void setPas(double pas) {
		this.pas = pas;
	}

}
