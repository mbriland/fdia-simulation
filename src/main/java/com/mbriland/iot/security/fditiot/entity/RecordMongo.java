package com.mbriland.iot.security.fditiot.entity;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;

public class RecordMongo {
	@Id
	public String id;
	
	ArrayList<PropertyRecord> propertyRecords;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<PropertyRecord> getPropertyRecords() {
		return propertyRecords;
	}

	public void setPropertyRecords(ArrayList<PropertyRecord> propertyRecords) {
		this.propertyRecords = propertyRecords;
	}

	
}
