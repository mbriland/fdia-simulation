package com.mbriland.iot.security.fditiot.antlr.impl;

public class Circle {
	
	private double x; //xcentre longitude
	private double y; //ycentre lattitude
	private double r; //rayon distance
	
	
	
	public Circle(double x, double y, double l) {
		super();
		this.x = x;
		this.y = y;
		this.r = l;
	}
	
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getR() {
		return r;
	}
	public void setR(double r) {
		this.r = r;
	}
	
	
}
