package com.mbriland.iot.security.fditiot.entity;

public class ConfigurationRecord {

	private String property;
	private String type;
	private String category;
	
	
	public ConfigurationRecord() {

	}
	
	
	public ConfigurationRecord(String property, String type, String category) {
		super();
		this.property = property;
		this.type = type;
		this.category = category;
	}
	
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	
	public String toJson() {
		//JSONObject jsonObject =new Jo
		
		return category;
		
	}
	
	
}
