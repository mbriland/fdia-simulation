package com.mbriland.iot.security.fditiot.antlr.impl;

public class AlterationCriterionArray extends AlterationCriterion {
	
	private int start;
	private int end;
	
	public AlterationCriterionArray(Object ident, Op op, Object expr, Object ident2, Op op2, Object expr2, int start,
			int end) {
		super(ident, op, expr);
		this.start = start;
		this.end = end;
	}
	
	public AlterationCriterionArray() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	
	
}
