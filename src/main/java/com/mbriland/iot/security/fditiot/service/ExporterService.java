package com.mbriland.iot.security.fditiot.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.GsonBuildConfig;
import com.mbriland.iot.security.fditiot.entity.Record;

@Service
public class ExporterService {
	
	@Autowired
	RecordService recordService;

	public void export(String collection) {

		List<Record> recordList = recordService.findAll(collection);
		
		JsonArray rootArray = new JsonArray();
		
		for (Record record : recordList) {
			JsonObject object = new JsonObject();
			for (Map.Entry<String, Object> entry: record.getProperty().entrySet()) {
				
				if(entry.getValue() instanceof String) {
					object.addProperty(entry.getKey(),entry.getValue().toString());
				}else if(entry.getValue() instanceof Double) {
					object.addProperty(entry.getKey(), Double.parseDouble(entry.getValue().toString()));
				}
			}
			rootArray.add(object);;
		}
		
		try {
			File file = new File("C:\\Users\\mbriland\\Desktop\\exporter\\"+collection+".json");
			FileWriter fileWriter = new FileWriter(file);
			Gson gson = new GsonBuilder()
					  .setPrettyPrinting()
					  .create();
			gson.toJson(rootArray, fileWriter);
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("exporting done");
		
	}

}
