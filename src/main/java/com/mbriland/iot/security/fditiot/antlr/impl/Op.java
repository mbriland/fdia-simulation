package com.mbriland.iot.security.fditiot.antlr.impl;

public enum Op {
	EQUAL,
	GREATERTHAN,
	LESSERTHAN,
	DIFFERENT,
	INCREMENT,
	MULTIPLY,
	ISINSIDECIRCLE
}
