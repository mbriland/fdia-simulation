package com.mbriland.iot.security.fditiot.service;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.mbriland.iot.security.fditiot.antlr.impl.AttenuationCriterion;
import com.mbriland.iot.security.fditiot.antlr.impl.Circle;
import com.mbriland.iot.security.fditiot.antlr.impl.SelectionCriterion;
import com.mbriland.iot.security.fditiot.entity.Record;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.result.UpdateResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class RecordService {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	MongoOperations mongoOperations;

	public void insertThingsInCollection(Record record, String collection) {
		mongoTemplate.insert(record, collection);
	}

	public void insertAll(List<Record> records, String collection) {
		BulkOperations bulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, Record.class, collection);
		((MappingMongoConverter) mongoTemplate.getConverter()).setMapKeyDotReplacement("*");
		bulk.insert(records);
		BulkWriteResult res = bulk.execute();
		System.out.println("inserted : " + res.getInsertedCount());
		System.out.println("list : " + records.size());

	}
	
	public List<Record> findAll(String collection) {
		return mongoTemplate.findAll(Record.class, collection);
	}

	public void updateThingsInCollection(Query query, Update update, String collection) {
		/*
		 * BulkOperations bulk = mongoTemplate.bulkOps(BulkMode.UNORDERED, Record.class,
		 * collection); bulk.updateMulti(query, update); bulk.execute();
		 */

		UpdateResult result = mongoTemplate.updateMulti(query, update, collection);
		System.out.println(result.toString());
	}

	public void delete(Query query, String collectionName) {
		mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		mongoTemplate.remove(query, collectionName);
	}

	public List<String> findDistinctThings(String collection) {
		List<String> list = mongoTemplate.findDistinct(new Query(Criteria.where("property.timestamp").gt(0)),
				"property.meter_code", collection, String.class);
		System.out.println("DONE FOR "+collection);
		return list;
	}

	public List<String> findDistinctThings(String collection, Query query) {
		List<String> list = mongoTemplate.findDistinct(query, "property.meter_code", collection, String.class);
		return list;
	}

	public List<String> findThingsInCircle(Circle c) {
		List<String> list = mongoTemplate.findDistinct(new Query(Criteria.where("property.timestamp").gt(0)),
				"property.meter_code", "juillet2019", String.class);
		List<Record> recordList = new ArrayList<Record>();
		for (String string : list) {
			recordList.add(mongoTemplate.findOne(new Query(Criteria.where("property.meter_code").is(string)),
					Record.class, "juillet2019"));
		}

		List<String> thingsInCircleList = new ArrayList<>();
		for (Record record : recordList) {
			String[] array = record.getProperty().get("location").toString().split(",");

			if (gps2m(Double.parseDouble(array[0]), Double.parseDouble(array[1]), c.getX(), c.getY()) <= c.getR()) {
				thingsInCircleList.add(record.getProperty().get("meter_code").toString());
			}

		}

		for (String string : thingsInCircleList) {
			System.out.println(string);
		}

		return thingsInCircleList;

	}

	private double gps2m(double lat_a, double lng_a, double lat_b, double lng_b) {
		double pk = (double) (180 / 3.14169);

		double a1 = lat_a / pk;
		double a2 = lng_a / pk;
		double b1 = lat_b / pk;
		double b2 = lng_b / pk;

		double t1 = (Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2));
		double t2 = (Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2));
		double t3 = (Math.sin(a1) * Math.sin(b1));
		double tt = Math.acos(t1 + t2 + t3);

		return 6366000 * tt;
	}

	public List<Record> queryNotInUse(List<SelectionCriterion> selectionCriterion, String collection) {

		ArrayList<Criteria> criteriaList = new ArrayList<>();

		for (SelectionCriterion selectionCriterion2 : selectionCriterion) {

			Criteria crit = Criteria.where("name").is(selectionCriterion2.getIdent()).and("value");
			switch (selectionCriterion2.getOp()) {
			case EQUAL:
				crit.is(selectionCriterion2.getExpr());
				break;
			case GREATERTHAN:
				crit.gt(selectionCriterion2.getExpr());
				break;
			case LESSERTHAN:
				crit.lt(selectionCriterion2.getExpr());
				break;
			case DIFFERENT:
				crit.ne(selectionCriterion2.getExpr());
				break;
			default:
				break;
			}
			criteriaList.add(Criteria.where("propertyRecords").elemMatch(crit));
		}

		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));

		query.with(Sort.by(Sort.Direction.ASC, "timestamp"));

		List<Record> lists = mongoTemplate.find(query, Record.class, collection);

		System.out.println("requete envoyé");
		for (Record record : lists) {
			System.out.println(record.toString());
		}

		return lists;

	}

	public List<Record> query(List<SelectionCriterion> selectionCriterion, String collection) {

		ArrayList<Criteria> criteriaList = new ArrayList<>();

		for (SelectionCriterion selectionCriterion2 : selectionCriterion) {

			Criteria crit;
			switch (selectionCriterion2.getOp()) {
			case EQUAL:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).is(selectionCriterion2.getExpr());
				break;
			case GREATERTHAN:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).gt(selectionCriterion2.getExpr());
				break;
			case LESSERTHAN:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).lt(selectionCriterion2.getExpr());
				break;
			case DIFFERENT:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).ne(selectionCriterion2.getExpr());
				break;
			default:
				crit = new Criteria();
				break;
			}
			criteriaList.add(crit);
		}

		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));

		query.with(Sort.by(Sort.Direction.ASC, "property.timestamp"));

		System.out.println(query.toString());
		List<Record> lists = mongoTemplate.find(query, Record.class, collection);

		System.out.println("requete envoyé");

		return lists;

	}

	public Query querySelectionBuilder(List<SelectionCriterion> selectionCriterion) {

		ArrayList<Criteria> criteriaList = new ArrayList<>();

		for (SelectionCriterion selectionCriterion2 : selectionCriterion) {
			System.out.println(selectionCriterion2);
			Criteria crit;
			switch (selectionCriterion2.getOp()) {
			case EQUAL:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).is(selectionCriterion2.getExpr());
				break;
			case GREATERTHAN:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).gt(selectionCriterion2.getExpr());
				break;
			case LESSERTHAN:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).lt(selectionCriterion2.getExpr());
				break;
			case DIFFERENT:
				crit = Criteria.where("property." + selectionCriterion2.getIdent()).ne(selectionCriterion2.getExpr());
				break;
			case ISINSIDECIRCLE:
				List<String> listInCircle = this.findThingsInCircle((Circle) selectionCriterion2.getExpr());
				ArrayList<Criteria> listCriteriaForCircle = new ArrayList<>();
				for (String string : listInCircle) {
					listCriteriaForCircle.add(Criteria.where("property.meter_code").is(string));
				}
				crit = new Criteria()
						.orOperator(listCriteriaForCircle.toArray(new Criteria[listCriteriaForCircle.size()]));
				break;
			default:
				crit = new Criteria();
				break;
			}
			criteriaList.add(crit);
		}

		Query query = new Query();
		query.addCriteria(new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()])));

		query.with(Sort.by(Sort.Direction.ASC, "property.timestamp"));
		System.out.println(query);
		return query;

	}

	public Map<String, Double> findDistance(List<String> thingsList, String locationProperty, String geolocation,
			String collection) {

		Map<String, Double> distanceThings = new HashMap<String, Double>();

		String[] geoloc = geolocation.split(",");

		List<Record> recordGeoloc = new ArrayList<Record>();
		for (String string : thingsList) {
			recordGeoloc.add(mongoTemplate.findOne(new Query(Criteria.where("property.meter_code").is(string)),
					Record.class, collection));
		}

		System.out.println(thingsList.size());

		System.out.println(recordGeoloc.size());

		for (Record record : recordGeoloc) {
			System.out.println(record);
			String[] array = record.getProperty().get(locationProperty).toString().split(",");

			distanceThings.put(record.getProperty().get("meter_code").toString(), gps2m(Double.parseDouble(array[0]),
					Double.parseDouble(array[1]), Double.parseDouble(geoloc[0]), Double.parseDouble(geoloc[1])));

		}

		return distanceThings;

	}

	public void incrementalEvolution(Query query, String collection, String geolocation, String property,
			Double increment, Double from, AttenuationCriterion attenuation) {

		List<Record> recordList;

		List<String> listThings = findDistinctThings(collection, query);

		if (attenuation != null) {

			Map<String, Double> distance = findDistance(listThings, "location", geolocation, collection);

			recordList = mongoTemplate.findAllAndRemove(query, Record.class, collection);

			System.out.println(distance);

			for (String string : listThings) {
				Double attenuationForce = (Double.parseDouble(attenuation.getExpr().toString()) * distance.get(string));
				Double i = from - attenuationForce;

				for (Record record : recordList) {
					if (record.getProperty().get("meter_code").equals(string)) {
						if (i > attenuationForce) {
							record.getProperty().replace(property, (double) record.getProperty().get(property) + i);
						}
						i += increment;
					}
				}
				i = from;
			}
		} else {

			recordList = mongoTemplate.findAllAndRemove(query, Record.class, collection);

			Double i = from;

			for (String string : listThings) {
				for (Record record : recordList) {
					if (record.getProperty().get("meter_code").equals(string)) {
						record.getProperty().replace(property, (double) record.getProperty().get(property) + i);
						i += increment;
					}
				}
				i = from;
			}
		}

		mongoTemplate.insert(recordList, collection);
	}

	
	public void unsetProperty(String property, SelectionCriterion select, String collectionName) {
		Query query = new Query();
		query.addCriteria(Criteria.where(select.getIdent()).is(select.getExpr()));
		Update update = new Update();
		update.unset(property);

		// run update operation
		mongoTemplate.updateMulti(query, update, Record.class, collectionName);
	}
	
}
