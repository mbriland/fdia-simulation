package com.mbriland.iot.security.fditiot.antlr.impl;

import java.util.List;

public class Execution {

	
	private EPrimitive primitive;
	private List<AlterationCriterion> alterationCriterion;
	private List<SelectionCriterion> selectionCriterion;
	private TimeFrame timeframe;
	
	
	public Execution(EPrimitive primitive, List<AlterationCriterion> alterationCriterion,
			List<SelectionCriterion> selectionCriterion, TimeFrame timeframe) {
		super();
		this.primitive = primitive;
		this.alterationCriterion = alterationCriterion;
		this.selectionCriterion = selectionCriterion;
		this.timeframe = timeframe;
	}
	
	public Execution() {
		// TODO Auto-generated constructor stub
	}

	public EPrimitive getPrimitive() {
		return primitive;
	}
	public void setPrimitive(EPrimitive primitive) {
		this.primitive = primitive;
	}
	public List<AlterationCriterion> getAlterationCriterion() {
		return alterationCriterion;
	}
	public void setAlterationCriterion(List<AlterationCriterion> alterationCriterion) {
		this.alterationCriterion = alterationCriterion;
	}
	public List<SelectionCriterion> getSelectionCriterion() {
		return selectionCriterion;
	}
	public void setSelectionCriterion(List<SelectionCriterion> selectionCriterion) {
		this.selectionCriterion = selectionCriterion;
	}
	public TimeFrame getTimeframe() {
		return timeframe;
	}
	public void setTimeframe(TimeFrame timeframe) {
		this.timeframe = timeframe;
	}

	@Override
	public String toString() {
		return "Execution [primitive=" + primitive + ", alterationCriterion=" + alterationCriterion
				+ ", selectionCriterion=" + selectionCriterion + ", timeframe=" + timeframe + "]";
	}
	
	
	
	
}
