package com.mbriland.iot.security.fditiot.config;

import static org.slf4j.LoggerFactory.getLogger;

import java.util.Objects;

import org.slf4j.Logger;

import com.mbriland.iot.security.fditiot.controller.ChartController;
import com.mbriland.iot.security.fditiot.controller.MappingJsonController;
import com.mbriland.iot.security.fditiot.controller.Webviewcontroller;
import com.mbriland.iot.security.fditiot.entity.JsonImportedWithProperty;
import com.mbriland.iot.security.fditiot.view.FxmlView;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Manages switching Scenes on the Primary Stage
 */
public class StageManager {

	private static final Logger LOG = getLogger(StageManager.class);
	private final Stage primaryStage;
	private final SpringFXMLLoader springFXMLLoader;
	private Stage stage;

	public StageManager(SpringFXMLLoader springFXMLLoader, Stage stage) {
		this.springFXMLLoader = springFXMLLoader;
		this.primaryStage = stage;
	}

	public void goToMappingJson(JsonImportedWithProperty jsonImportedWithProperty) {
		Parent rootNode = null;
		try {

			FXMLLoader loader = springFXMLLoader.getFxmlLoader(FxmlView.MAPPINGJSON.getFxmlFile());
			rootNode = loader.load();
			MappingJsonController controller = loader.getController();
			controller.initData(jsonImportedWithProperty);

			Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
		} catch (Exception exception) {
			logAndExit("Unable to load FXML view : " + FxmlView.MAPPINGJSON.getFxmlFile() + exception.getMessage(),
					exception);
		}

		show(rootNode, FxmlView.MAPPINGJSON.getTitle());

	}

	public void goToChart(String scenar) {
		Parent rootNode = null;
		try {

			FXMLLoader loader = springFXMLLoader.getFxmlLoader(FxmlView.CHART.getFxmlFile());
			rootNode = loader.load();
			ChartController controller = loader.getController();
			controller.initData(scenar);

			Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
		} catch (Exception exception) {
			logAndExit("Unable to load FXML view : " + FxmlView.CHART.getFxmlFile() + exception.getMessage(),
					exception);
			exception.printStackTrace();
		}

		showNew(rootNode, FxmlView.CHART.getTitle());

	}

	public void goToWebView(String scenar) {
		Parent rootNode = null;
		try {

			FXMLLoader loader = springFXMLLoader.getFxmlLoader(FxmlView.WEBVIEW.getFxmlFile());
			rootNode = loader.load();
			Webviewcontroller controller = loader.getController();
			controller.initData(scenar);

			Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
		} catch (Exception exception) {
			logAndExit("Unable to load FXML view : " + FxmlView.WEBVIEW.getFxmlFile() + exception.getMessage(),
					exception);
			exception.printStackTrace();
		}

		showNew(rootNode, FxmlView.WEBVIEW.getTitle());

	}

	public void switchScene(final FxmlView view) {
		Parent viewRootNodeHierarchy = loadViewNodeHierarchy(view.getFxmlFile());
		show(viewRootNodeHierarchy, view.getTitle());
	}

	private void show(final Parent rootnode, String title) {
		Scene scene = prepareScene(rootnode);
		scene.getStylesheets().add("/styles/bootstrap3.css");
		scene.getStylesheets().add("/styles/styles.css");

		// primaryStage.initStyle(StageStyle.TRANSPARENT);
		primaryStage.setTitle(title);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();		
		try {
			primaryStage.show();
		} catch (Exception exception) {
			logAndExit("Unable to show scene for title" + title, exception);
		}
	}

	private void showNew(final Parent rootnode, String title) {
		stage = new Stage();
		Scene scene = prepareSceneNew(rootnode);
		scene.getStylesheets().add("/styles/bootstrap3.css");
		scene.getStylesheets().add("/styles/styles.css");

		// primaryStage.initStyle(StageStyle.TRANSPARENT);
		stage.setTitle(title);
		stage.setScene(scene);
		stage.sizeToScene();
		stage.centerOnScreen();

		try {
			stage.show();
		} catch (Exception exception) {
			logAndExit("Unable to show scene for title" + title, exception);
		}
	}

	private Scene prepareSceneNew(Parent rootnode) {
		Scene scene = stage.getScene();

		if (scene == null) {
			scene = new Scene(rootnode);
		}
		scene.setRoot(rootnode);
		return scene;
	}

	private Scene prepareScene(Parent rootnode) {
		Scene scene = primaryStage.getScene();

		if (scene == null) {
			scene = new Scene(rootnode);
		}
		scene.setRoot(rootnode);
		return scene;
	}

	/**
	 * Loads the object hierarchy from a FXML document and returns to root node of
	 * that hierarchy.
	 *
	 * @return Parent root node of the FXML document hierarchy
	 */
	private Parent loadViewNodeHierarchy(String fxmlFilePath) {
		Parent rootNode = null;
		try {
			rootNode = springFXMLLoader.load(fxmlFilePath);
			Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
		} catch (Exception exception) {
			logAndExit("Unable to load FXML view" + fxmlFilePath, exception);
		}
		return rootNode;
	}

	private void logAndExit(String errorMsg, Exception exception) {
		LOG.error(errorMsg, exception, exception.getCause());
		Platform.exit();
	}

}
