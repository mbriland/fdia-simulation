grammar fd2iot;

SCENARIO: 'scenario';
TICKER:'ticker';
CREATE:'create';
ALTER:'alter';
COPY:'copy';
DELETE:'delete';
THINGS:'things';
SET:'set';
FROM:'from';
TO:'to';
OF:'of';
ISINSIDECIRCLE:'isInsideCircle';
WITH: 'with';
ATTENUATION: 'attenuation';
GEOLOCATION: 'geolocation';
ARRAY: 'array';
EQUAL_SYMBOL:'=';
GREATER_SYMBOL:'>';
LESS_SYMBOL:'<';
DIFFERENT_SYMBOL:'!=';
EXCLAMATION_SYMBOL:'!';
INCREMENT_SYMBOL:'+=';
MULTIPLYOFFSET_SYMBOL:'*=';
WHERE:'where';
SEMI: ';';
COMMA: ',';
AND:'and';
LEFT_BRACKET:'(';
RIGHT_BRACKET:')';
LEFT_ARROW:'->';

STRING_LITERAL: DQUOTA_STRING | SQUOTA_STRING | BQUOTA_STRING;
DECIMAL_LITERAL: ('-')? DEC_DIGIT+;
HEXADECIMAL_LITERAL: 'X' '\'' (HEX_DIGIT HEX_DIGIT)+ '\''
                    | '0X' HEX_DIGIT+;
ID:  ID_LITERAL;

REAL_LITERAL:                        ('-')?((DEC_DIGIT+)? '.' DEC_DIGIT+
                                     | DEC_DIGIT+ '.' EXPONENT_NUM_PART
                                     | (DEC_DIGIT+)? '.' (DEC_DIGIT+ EXPONENT_NUM_PART)
                                     | DEC_DIGIT+ EXPONENT_NUM_PART);

fragment ID_LITERAL:                 [A-Za-z_$*]*?[A-Za-z_$*]+?[A-Za-z_$*0-9]*;
fragment DQUOTA_STRING:              '"' ( '\\'. | '""' | ~('"'| '\\') )* '"';
fragment SQUOTA_STRING:              '\'' ('\\'. | '\'\'' | ~('\'' | '\\'))* '\'';
fragment BQUOTA_STRING:              '`' ( '\\'. | '``' | ~('`'|'\\'))* '`';
fragment HEX_DIGIT:                  [0-9A-F];
fragment DEC_DIGIT:                  [0-9];
fragment BIT_STRING_L:               'B' '\'' [01]+ '\'';
fragment EXPONENT_NUM_PART:          'E' [-+]? DEC_DIGIT+;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newline


root: scenarioDeclaration executionList;

scenarioDeclaration:
	SCENARIO STRING_LITERAL TICKER DECIMAL_LITERAL (GEOLOCATION LEFT_BRACKET REAL_LITERAL COMMA REAL_LITERAL RIGHT_BRACKET)
;

executionList:
	execution SEMI (execution SEMI)*
;

execution: ((CREATE THINGS alterationCriteria timeframe)
	|  (ALTER THINGS selectionCriteria alterationCriteria timeframe)
	| (DELETE THINGS selectionCriteria timeframe)
	| (COPY THINGS selectionCriteria alterationCriteria timeframe))
	;
	
selectionCriteria: WHERE selectionCriterion (AND selectionCriterion)*;

selectionCriterion: ID EQUAL_SYMBOL type #equalSelection
	| ID GREATER_SYMBOL type #greaterThanSelection
	| ID LESS_SYMBOL type  #lessThanSelection
	| ID DIFFERENT_SYMBOL type #differentSelection
	| ID ISINSIDECIRCLE LEFT_BRACKET REAL_LITERAL COMMA REAL_LITERAL COMMA DECIMAL_LITERAL RIGHT_BRACKET #isInsideCircleSelection
	;
	
timeframe: FROM DECIMAL_LITERAL TO DECIMAL_LITERAL;

attenuationCriteria: WITH ATTENUATION OF REAL_LITERAL;

alterationCriteria: SET alterationCriterion (AND alterationCriterion)*;

alterationCriterion: ID EQUAL_SYMBOL type #affectation
|ID EQUAL_SYMBOL evol #evolution
|ID INCREMENT_SYMBOL REAL_LITERAL #offset
|ID MULTIPLYOFFSET_SYMBOL REAL_LITERAL #multiplyOffset
|ID INCREMENT_SYMBOL evol attenuationCriteria? #evolutionIncrementation
|array INCREMENT_SYMBOL REAL_LITERAL #arrayIncrementation
;

evol: LEFT_BRACKET REAL_LITERAL LEFT_ARROW REAL_LITERAL COMMA REAL_LITERAL RIGHT_BRACKET;

array: ARRAY LEFT_BRACKET ID COMMA DECIMAL_LITERAL COMMA DECIMAL_LITERAL RIGHT_BRACKET;

type:ID #identType
|STRING_LITERAL #stringLiteralType
|DECIMAL_LITERAL #decimalLitealType
|REAL_LITERAL #realLiteralType
|HEXADECIMAL_LITERAL #hexaDecimalType;