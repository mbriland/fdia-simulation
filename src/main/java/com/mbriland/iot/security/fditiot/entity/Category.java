package com.mbriland.iot.security.fditiot.entity;

public enum Category {
	 
	   TEMPERATURE("TEMP", "Temperature"), 
	   HUMIDITY("HUMID", "Humidity"), 
	   IDENTIFICATION("IDENT","Identification"),
	   TEMPS("TEMPS","Temps"),
	   NOISE("NOISE","noise"),
	   LOCATION("LOCATION","location"),
	   DEFAULT("DEFAULT","default");
	 
	   private String code;
	   private String text;
	   
	 
	   private Category(String code, String text) {
		this.code = code;
		this.text = text;
	}
	   
	   

	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}


	public static Category getByCode(String categoryCode) {
	       for (Category c : Category.values()) {
	           if (c.code.equals(categoryCode)) {
	               return c;
	           }
	       }
	       return null;
	   }
	 
	   @Override
	   public String toString() {
	       return this.text;
	   }
	 
	}