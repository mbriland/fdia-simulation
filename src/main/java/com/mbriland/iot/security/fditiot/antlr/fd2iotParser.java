// Generated from fd2iot.g4 by ANTLR 4.8
package com.mbriland.iot.security.fditiot.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class fd2iotParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		SCENARIO=1, TICKER=2, CREATE=3, ALTER=4, COPY=5, DELETE=6, THINGS=7, SET=8, 
		FROM=9, TO=10, OF=11, ISINSIDECIRCLE=12, WITH=13, ATTENUATION=14, GEOLOCATION=15, 
		ARRAY=16, EQUAL_SYMBOL=17, GREATER_SYMBOL=18, LESS_SYMBOL=19, DIFFERENT_SYMBOL=20, 
		EXCLAMATION_SYMBOL=21, INCREMENT_SYMBOL=22, MULTIPLYOFFSET_SYMBOL=23, 
		WHERE=24, SEMI=25, COMMA=26, AND=27, LEFT_BRACKET=28, RIGHT_BRACKET=29, 
		LEFT_ARROW=30, STRING_LITERAL=31, DECIMAL_LITERAL=32, HEXADECIMAL_LITERAL=33, 
		ID=34, REAL_LITERAL=35, WS=36;
	public static final int
		RULE_root = 0, RULE_scenarioDeclaration = 1, RULE_executionList = 2, RULE_execution = 3, 
		RULE_selectionCriteria = 4, RULE_selectionCriterion = 5, RULE_timeframe = 6, 
		RULE_attenuationCriteria = 7, RULE_alterationCriteria = 8, RULE_alterationCriterion = 9, 
		RULE_evol = 10, RULE_array = 11, RULE_type = 12;
	private static String[] makeRuleNames() {
		return new String[] {
			"root", "scenarioDeclaration", "executionList", "execution", "selectionCriteria", 
			"selectionCriterion", "timeframe", "attenuationCriteria", "alterationCriteria", 
			"alterationCriterion", "evol", "array", "type"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'scenario'", "'ticker'", "'create'", "'alter'", "'copy'", "'delete'", 
			"'things'", "'set'", "'from'", "'to'", "'of'", "'isInsideCircle'", "'with'", 
			"'attenuation'", "'geolocation'", "'array'", "'='", "'>'", "'<'", "'!='", 
			"'!'", "'+='", "'*='", "'where'", "';'", "','", "'and'", "'('", "')'", 
			"'->'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "SCENARIO", "TICKER", "CREATE", "ALTER", "COPY", "DELETE", "THINGS", 
			"SET", "FROM", "TO", "OF", "ISINSIDECIRCLE", "WITH", "ATTENUATION", "GEOLOCATION", 
			"ARRAY", "EQUAL_SYMBOL", "GREATER_SYMBOL", "LESS_SYMBOL", "DIFFERENT_SYMBOL", 
			"EXCLAMATION_SYMBOL", "INCREMENT_SYMBOL", "MULTIPLYOFFSET_SYMBOL", "WHERE", 
			"SEMI", "COMMA", "AND", "LEFT_BRACKET", "RIGHT_BRACKET", "LEFT_ARROW", 
			"STRING_LITERAL", "DECIMAL_LITERAL", "HEXADECIMAL_LITERAL", "ID", "REAL_LITERAL", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "fd2iot.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public fd2iotParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class RootContext extends ParserRuleContext {
		public ScenarioDeclarationContext scenarioDeclaration() {
			return getRuleContext(ScenarioDeclarationContext.class,0);
		}
		public ExecutionListContext executionList() {
			return getRuleContext(ExecutionListContext.class,0);
		}
		public RootContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_root; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterRoot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitRoot(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitRoot(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RootContext root() throws RecognitionException {
		RootContext _localctx = new RootContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_root);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			scenarioDeclaration();
			setState(27);
			executionList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScenarioDeclarationContext extends ParserRuleContext {
		public TerminalNode SCENARIO() { return getToken(fd2iotParser.SCENARIO, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(fd2iotParser.STRING_LITERAL, 0); }
		public TerminalNode TICKER() { return getToken(fd2iotParser.TICKER, 0); }
		public TerminalNode DECIMAL_LITERAL() { return getToken(fd2iotParser.DECIMAL_LITERAL, 0); }
		public TerminalNode GEOLOCATION() { return getToken(fd2iotParser.GEOLOCATION, 0); }
		public TerminalNode LEFT_BRACKET() { return getToken(fd2iotParser.LEFT_BRACKET, 0); }
		public List<TerminalNode> REAL_LITERAL() { return getTokens(fd2iotParser.REAL_LITERAL); }
		public TerminalNode REAL_LITERAL(int i) {
			return getToken(fd2iotParser.REAL_LITERAL, i);
		}
		public TerminalNode COMMA() { return getToken(fd2iotParser.COMMA, 0); }
		public TerminalNode RIGHT_BRACKET() { return getToken(fd2iotParser.RIGHT_BRACKET, 0); }
		public ScenarioDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scenarioDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterScenarioDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitScenarioDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitScenarioDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ScenarioDeclarationContext scenarioDeclaration() throws RecognitionException {
		ScenarioDeclarationContext _localctx = new ScenarioDeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_scenarioDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29);
			match(SCENARIO);
			setState(30);
			match(STRING_LITERAL);
			setState(31);
			match(TICKER);
			setState(32);
			match(DECIMAL_LITERAL);
			{
			setState(33);
			match(GEOLOCATION);
			setState(34);
			match(LEFT_BRACKET);
			setState(35);
			match(REAL_LITERAL);
			setState(36);
			match(COMMA);
			setState(37);
			match(REAL_LITERAL);
			setState(38);
			match(RIGHT_BRACKET);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExecutionListContext extends ParserRuleContext {
		public List<ExecutionContext> execution() {
			return getRuleContexts(ExecutionContext.class);
		}
		public ExecutionContext execution(int i) {
			return getRuleContext(ExecutionContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(fd2iotParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(fd2iotParser.SEMI, i);
		}
		public ExecutionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_executionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterExecutionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitExecutionList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitExecutionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExecutionListContext executionList() throws RecognitionException {
		ExecutionListContext _localctx = new ExecutionListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_executionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40);
			execution();
			setState(41);
			match(SEMI);
			setState(47);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CREATE) | (1L << ALTER) | (1L << COPY) | (1L << DELETE))) != 0)) {
				{
				{
				setState(42);
				execution();
				setState(43);
				match(SEMI);
				}
				}
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExecutionContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(fd2iotParser.CREATE, 0); }
		public TerminalNode THINGS() { return getToken(fd2iotParser.THINGS, 0); }
		public AlterationCriteriaContext alterationCriteria() {
			return getRuleContext(AlterationCriteriaContext.class,0);
		}
		public TimeframeContext timeframe() {
			return getRuleContext(TimeframeContext.class,0);
		}
		public TerminalNode ALTER() { return getToken(fd2iotParser.ALTER, 0); }
		public SelectionCriteriaContext selectionCriteria() {
			return getRuleContext(SelectionCriteriaContext.class,0);
		}
		public TerminalNode DELETE() { return getToken(fd2iotParser.DELETE, 0); }
		public TerminalNode COPY() { return getToken(fd2iotParser.COPY, 0); }
		public ExecutionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_execution; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterExecution(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitExecution(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitExecution(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExecutionContext execution() throws RecognitionException {
		ExecutionContext _localctx = new ExecutionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_execution);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case CREATE:
				{
				{
				setState(50);
				match(CREATE);
				setState(51);
				match(THINGS);
				setState(52);
				alterationCriteria();
				setState(53);
				timeframe();
				}
				}
				break;
			case ALTER:
				{
				{
				setState(55);
				match(ALTER);
				setState(56);
				match(THINGS);
				setState(57);
				selectionCriteria();
				setState(58);
				alterationCriteria();
				setState(59);
				timeframe();
				}
				}
				break;
			case DELETE:
				{
				{
				setState(61);
				match(DELETE);
				setState(62);
				match(THINGS);
				setState(63);
				selectionCriteria();
				setState(64);
				timeframe();
				}
				}
				break;
			case COPY:
				{
				{
				setState(66);
				match(COPY);
				setState(67);
				match(THINGS);
				setState(68);
				selectionCriteria();
				setState(69);
				alterationCriteria();
				setState(70);
				timeframe();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectionCriteriaContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(fd2iotParser.WHERE, 0); }
		public List<SelectionCriterionContext> selectionCriterion() {
			return getRuleContexts(SelectionCriterionContext.class);
		}
		public SelectionCriterionContext selectionCriterion(int i) {
			return getRuleContext(SelectionCriterionContext.class,i);
		}
		public List<TerminalNode> AND() { return getTokens(fd2iotParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(fd2iotParser.AND, i);
		}
		public SelectionCriteriaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectionCriteria; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterSelectionCriteria(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitSelectionCriteria(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitSelectionCriteria(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectionCriteriaContext selectionCriteria() throws RecognitionException {
		SelectionCriteriaContext _localctx = new SelectionCriteriaContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_selectionCriteria);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74);
			match(WHERE);
			setState(75);
			selectionCriterion();
			setState(80);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND) {
				{
				{
				setState(76);
				match(AND);
				setState(77);
				selectionCriterion();
				}
				}
				setState(82);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectionCriterionContext extends ParserRuleContext {
		public SelectionCriterionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectionCriterion; }
	 
		public SelectionCriterionContext() { }
		public void copyFrom(SelectionCriterionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LessThanSelectionContext extends SelectionCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode LESS_SYMBOL() { return getToken(fd2iotParser.LESS_SYMBOL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public LessThanSelectionContext(SelectionCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterLessThanSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitLessThanSelection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitLessThanSelection(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DifferentSelectionContext extends SelectionCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode DIFFERENT_SYMBOL() { return getToken(fd2iotParser.DIFFERENT_SYMBOL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public DifferentSelectionContext(SelectionCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterDifferentSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitDifferentSelection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitDifferentSelection(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualSelectionContext extends SelectionCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode EQUAL_SYMBOL() { return getToken(fd2iotParser.EQUAL_SYMBOL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public EqualSelectionContext(SelectionCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterEqualSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitEqualSelection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitEqualSelection(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GreaterThanSelectionContext extends SelectionCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode GREATER_SYMBOL() { return getToken(fd2iotParser.GREATER_SYMBOL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public GreaterThanSelectionContext(SelectionCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterGreaterThanSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitGreaterThanSelection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitGreaterThanSelection(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsInsideCircleSelectionContext extends SelectionCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode ISINSIDECIRCLE() { return getToken(fd2iotParser.ISINSIDECIRCLE, 0); }
		public TerminalNode LEFT_BRACKET() { return getToken(fd2iotParser.LEFT_BRACKET, 0); }
		public List<TerminalNode> REAL_LITERAL() { return getTokens(fd2iotParser.REAL_LITERAL); }
		public TerminalNode REAL_LITERAL(int i) {
			return getToken(fd2iotParser.REAL_LITERAL, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(fd2iotParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(fd2iotParser.COMMA, i);
		}
		public TerminalNode DECIMAL_LITERAL() { return getToken(fd2iotParser.DECIMAL_LITERAL, 0); }
		public TerminalNode RIGHT_BRACKET() { return getToken(fd2iotParser.RIGHT_BRACKET, 0); }
		public IsInsideCircleSelectionContext(SelectionCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterIsInsideCircleSelection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitIsInsideCircleSelection(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitIsInsideCircleSelection(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SelectionCriterionContext selectionCriterion() throws RecognitionException {
		SelectionCriterionContext _localctx = new SelectionCriterionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_selectionCriterion);
		try {
			setState(104);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				_localctx = new EqualSelectionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(83);
				match(ID);
				setState(84);
				match(EQUAL_SYMBOL);
				setState(85);
				type();
				}
				break;
			case 2:
				_localctx = new GreaterThanSelectionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(86);
				match(ID);
				setState(87);
				match(GREATER_SYMBOL);
				setState(88);
				type();
				}
				break;
			case 3:
				_localctx = new LessThanSelectionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(89);
				match(ID);
				setState(90);
				match(LESS_SYMBOL);
				setState(91);
				type();
				}
				break;
			case 4:
				_localctx = new DifferentSelectionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(92);
				match(ID);
				setState(93);
				match(DIFFERENT_SYMBOL);
				setState(94);
				type();
				}
				break;
			case 5:
				_localctx = new IsInsideCircleSelectionContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(95);
				match(ID);
				setState(96);
				match(ISINSIDECIRCLE);
				setState(97);
				match(LEFT_BRACKET);
				setState(98);
				match(REAL_LITERAL);
				setState(99);
				match(COMMA);
				setState(100);
				match(REAL_LITERAL);
				setState(101);
				match(COMMA);
				setState(102);
				match(DECIMAL_LITERAL);
				setState(103);
				match(RIGHT_BRACKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeframeContext extends ParserRuleContext {
		public TerminalNode FROM() { return getToken(fd2iotParser.FROM, 0); }
		public List<TerminalNode> DECIMAL_LITERAL() { return getTokens(fd2iotParser.DECIMAL_LITERAL); }
		public TerminalNode DECIMAL_LITERAL(int i) {
			return getToken(fd2iotParser.DECIMAL_LITERAL, i);
		}
		public TerminalNode TO() { return getToken(fd2iotParser.TO, 0); }
		public TimeframeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeframe; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterTimeframe(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitTimeframe(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitTimeframe(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeframeContext timeframe() throws RecognitionException {
		TimeframeContext _localctx = new TimeframeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_timeframe);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			match(FROM);
			setState(107);
			match(DECIMAL_LITERAL);
			setState(108);
			match(TO);
			setState(109);
			match(DECIMAL_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttenuationCriteriaContext extends ParserRuleContext {
		public TerminalNode WITH() { return getToken(fd2iotParser.WITH, 0); }
		public TerminalNode ATTENUATION() { return getToken(fd2iotParser.ATTENUATION, 0); }
		public TerminalNode OF() { return getToken(fd2iotParser.OF, 0); }
		public TerminalNode REAL_LITERAL() { return getToken(fd2iotParser.REAL_LITERAL, 0); }
		public AttenuationCriteriaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attenuationCriteria; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterAttenuationCriteria(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitAttenuationCriteria(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitAttenuationCriteria(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttenuationCriteriaContext attenuationCriteria() throws RecognitionException {
		AttenuationCriteriaContext _localctx = new AttenuationCriteriaContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_attenuationCriteria);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(WITH);
			setState(112);
			match(ATTENUATION);
			setState(113);
			match(OF);
			setState(114);
			match(REAL_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AlterationCriteriaContext extends ParserRuleContext {
		public TerminalNode SET() { return getToken(fd2iotParser.SET, 0); }
		public List<AlterationCriterionContext> alterationCriterion() {
			return getRuleContexts(AlterationCriterionContext.class);
		}
		public AlterationCriterionContext alterationCriterion(int i) {
			return getRuleContext(AlterationCriterionContext.class,i);
		}
		public List<TerminalNode> AND() { return getTokens(fd2iotParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(fd2iotParser.AND, i);
		}
		public AlterationCriteriaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alterationCriteria; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterAlterationCriteria(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitAlterationCriteria(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitAlterationCriteria(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AlterationCriteriaContext alterationCriteria() throws RecognitionException {
		AlterationCriteriaContext _localctx = new AlterationCriteriaContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_alterationCriteria);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(116);
			match(SET);
			setState(117);
			alterationCriterion();
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND) {
				{
				{
				setState(118);
				match(AND);
				setState(119);
				alterationCriterion();
				}
				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AlterationCriterionContext extends ParserRuleContext {
		public AlterationCriterionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alterationCriterion; }
	 
		public AlterationCriterionContext() { }
		public void copyFrom(AlterationCriterionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class EvolutionIncrementationContext extends AlterationCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode INCREMENT_SYMBOL() { return getToken(fd2iotParser.INCREMENT_SYMBOL, 0); }
		public EvolContext evol() {
			return getRuleContext(EvolContext.class,0);
		}
		public AttenuationCriteriaContext attenuationCriteria() {
			return getRuleContext(AttenuationCriteriaContext.class,0);
		}
		public EvolutionIncrementationContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterEvolutionIncrementation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitEvolutionIncrementation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitEvolutionIncrementation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OffsetContext extends AlterationCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode INCREMENT_SYMBOL() { return getToken(fd2iotParser.INCREMENT_SYMBOL, 0); }
		public TerminalNode REAL_LITERAL() { return getToken(fd2iotParser.REAL_LITERAL, 0); }
		public OffsetContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterOffset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitOffset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitOffset(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayIncrementationContext extends AlterationCriterionContext {
		public ArrayContext array() {
			return getRuleContext(ArrayContext.class,0);
		}
		public TerminalNode INCREMENT_SYMBOL() { return getToken(fd2iotParser.INCREMENT_SYMBOL, 0); }
		public TerminalNode REAL_LITERAL() { return getToken(fd2iotParser.REAL_LITERAL, 0); }
		public ArrayIncrementationContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterArrayIncrementation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitArrayIncrementation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitArrayIncrementation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplyOffsetContext extends AlterationCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode MULTIPLYOFFSET_SYMBOL() { return getToken(fd2iotParser.MULTIPLYOFFSET_SYMBOL, 0); }
		public TerminalNode REAL_LITERAL() { return getToken(fd2iotParser.REAL_LITERAL, 0); }
		public MultiplyOffsetContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterMultiplyOffset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitMultiplyOffset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitMultiplyOffset(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EvolutionContext extends AlterationCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode EQUAL_SYMBOL() { return getToken(fd2iotParser.EQUAL_SYMBOL, 0); }
		public EvolContext evol() {
			return getRuleContext(EvolContext.class,0);
		}
		public EvolutionContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterEvolution(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitEvolution(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitEvolution(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AffectationContext extends AlterationCriterionContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public TerminalNode EQUAL_SYMBOL() { return getToken(fd2iotParser.EQUAL_SYMBOL, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public AffectationContext(AlterationCriterionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterAffectation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitAffectation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitAffectation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AlterationCriterionContext alterationCriterion() throws RecognitionException {
		AlterationCriterionContext _localctx = new AlterationCriterionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_alterationCriterion);
		int _la;
		try {
			setState(147);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				_localctx = new AffectationContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(125);
				match(ID);
				setState(126);
				match(EQUAL_SYMBOL);
				setState(127);
				type();
				}
				break;
			case 2:
				_localctx = new EvolutionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(128);
				match(ID);
				setState(129);
				match(EQUAL_SYMBOL);
				setState(130);
				evol();
				}
				break;
			case 3:
				_localctx = new OffsetContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(131);
				match(ID);
				setState(132);
				match(INCREMENT_SYMBOL);
				setState(133);
				match(REAL_LITERAL);
				}
				break;
			case 4:
				_localctx = new MultiplyOffsetContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(134);
				match(ID);
				setState(135);
				match(MULTIPLYOFFSET_SYMBOL);
				setState(136);
				match(REAL_LITERAL);
				}
				break;
			case 5:
				_localctx = new EvolutionIncrementationContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(137);
				match(ID);
				setState(138);
				match(INCREMENT_SYMBOL);
				setState(139);
				evol();
				setState(141);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==WITH) {
					{
					setState(140);
					attenuationCriteria();
					}
				}

				}
				break;
			case 6:
				_localctx = new ArrayIncrementationContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(143);
				array();
				setState(144);
				match(INCREMENT_SYMBOL);
				setState(145);
				match(REAL_LITERAL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EvolContext extends ParserRuleContext {
		public TerminalNode LEFT_BRACKET() { return getToken(fd2iotParser.LEFT_BRACKET, 0); }
		public List<TerminalNode> REAL_LITERAL() { return getTokens(fd2iotParser.REAL_LITERAL); }
		public TerminalNode REAL_LITERAL(int i) {
			return getToken(fd2iotParser.REAL_LITERAL, i);
		}
		public TerminalNode LEFT_ARROW() { return getToken(fd2iotParser.LEFT_ARROW, 0); }
		public TerminalNode COMMA() { return getToken(fd2iotParser.COMMA, 0); }
		public TerminalNode RIGHT_BRACKET() { return getToken(fd2iotParser.RIGHT_BRACKET, 0); }
		public EvolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_evol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterEvol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitEvol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitEvol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EvolContext evol() throws RecognitionException {
		EvolContext _localctx = new EvolContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_evol);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			match(LEFT_BRACKET);
			setState(150);
			match(REAL_LITERAL);
			setState(151);
			match(LEFT_ARROW);
			setState(152);
			match(REAL_LITERAL);
			setState(153);
			match(COMMA);
			setState(154);
			match(REAL_LITERAL);
			setState(155);
			match(RIGHT_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayContext extends ParserRuleContext {
		public TerminalNode ARRAY() { return getToken(fd2iotParser.ARRAY, 0); }
		public TerminalNode LEFT_BRACKET() { return getToken(fd2iotParser.LEFT_BRACKET, 0); }
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public List<TerminalNode> COMMA() { return getTokens(fd2iotParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(fd2iotParser.COMMA, i);
		}
		public List<TerminalNode> DECIMAL_LITERAL() { return getTokens(fd2iotParser.DECIMAL_LITERAL); }
		public TerminalNode DECIMAL_LITERAL(int i) {
			return getToken(fd2iotParser.DECIMAL_LITERAL, i);
		}
		public TerminalNode RIGHT_BRACKET() { return getToken(fd2iotParser.RIGHT_BRACKET, 0); }
		public ArrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitArray(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitArray(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayContext array() throws RecognitionException {
		ArrayContext _localctx = new ArrayContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(ARRAY);
			setState(158);
			match(LEFT_BRACKET);
			setState(159);
			match(ID);
			setState(160);
			match(COMMA);
			setState(161);
			match(DECIMAL_LITERAL);
			setState(162);
			match(COMMA);
			setState(163);
			match(DECIMAL_LITERAL);
			setState(164);
			match(RIGHT_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
	 
		public TypeContext() { }
		public void copyFrom(TypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class HexaDecimalTypeContext extends TypeContext {
		public TerminalNode HEXADECIMAL_LITERAL() { return getToken(fd2iotParser.HEXADECIMAL_LITERAL, 0); }
		public HexaDecimalTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterHexaDecimalType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitHexaDecimalType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitHexaDecimalType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentTypeContext extends TypeContext {
		public TerminalNode ID() { return getToken(fd2iotParser.ID, 0); }
		public IdentTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterIdentType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitIdentType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitIdentType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringLiteralTypeContext extends TypeContext {
		public TerminalNode STRING_LITERAL() { return getToken(fd2iotParser.STRING_LITERAL, 0); }
		public StringLiteralTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterStringLiteralType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitStringLiteralType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitStringLiteralType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealLiteralTypeContext extends TypeContext {
		public TerminalNode REAL_LITERAL() { return getToken(fd2iotParser.REAL_LITERAL, 0); }
		public RealLiteralTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterRealLiteralType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitRealLiteralType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitRealLiteralType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DecimalLitealTypeContext extends TypeContext {
		public TerminalNode DECIMAL_LITERAL() { return getToken(fd2iotParser.DECIMAL_LITERAL, 0); }
		public DecimalLitealTypeContext(TypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).enterDecimalLitealType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof fd2iotListener ) ((fd2iotListener)listener).exitDecimalLitealType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof fd2iotVisitor ) return ((fd2iotVisitor<? extends T>)visitor).visitDecimalLitealType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_type);
		try {
			setState(171);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new IdentTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(166);
				match(ID);
				}
				break;
			case STRING_LITERAL:
				_localctx = new StringLiteralTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(167);
				match(STRING_LITERAL);
				}
				break;
			case DECIMAL_LITERAL:
				_localctx = new DecimalLitealTypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(168);
				match(DECIMAL_LITERAL);
				}
				break;
			case REAL_LITERAL:
				_localctx = new RealLiteralTypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(169);
				match(REAL_LITERAL);
				}
				break;
			case HEXADECIMAL_LITERAL:
				_localctx = new HexaDecimalTypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(170);
				match(HEXADECIMAL_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3&\u00b0\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\7\4\60\n\4\f\4\16\4\63\13\4\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\5\5K\n\5\3\6\3\6\3\6\3\6\7\6Q\n\6\f\6\16\6T\13\6\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\5\7k\n\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3"+
		"\n\7\n{\n\n\f\n\16\n~\13\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u0090\n\13\3\13\3\13\3\13\3\13"+
		"\5\13\u0096\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\5\16\u00ae\n\16\3\16\2\2\17\2"+
		"\4\6\b\n\f\16\20\22\24\26\30\32\2\2\2\u00b6\2\34\3\2\2\2\4\37\3\2\2\2"+
		"\6*\3\2\2\2\bJ\3\2\2\2\nL\3\2\2\2\fj\3\2\2\2\16l\3\2\2\2\20q\3\2\2\2\22"+
		"v\3\2\2\2\24\u0095\3\2\2\2\26\u0097\3\2\2\2\30\u009f\3\2\2\2\32\u00ad"+
		"\3\2\2\2\34\35\5\4\3\2\35\36\5\6\4\2\36\3\3\2\2\2\37 \7\3\2\2 !\7!\2\2"+
		"!\"\7\4\2\2\"#\7\"\2\2#$\7\21\2\2$%\7\36\2\2%&\7%\2\2&\'\7\34\2\2\'(\7"+
		"%\2\2()\7\37\2\2)\5\3\2\2\2*+\5\b\5\2+\61\7\33\2\2,-\5\b\5\2-.\7\33\2"+
		"\2.\60\3\2\2\2/,\3\2\2\2\60\63\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\7"+
		"\3\2\2\2\63\61\3\2\2\2\64\65\7\5\2\2\65\66\7\t\2\2\66\67\5\22\n\2\678"+
		"\5\16\b\28K\3\2\2\29:\7\6\2\2:;\7\t\2\2;<\5\n\6\2<=\5\22\n\2=>\5\16\b"+
		"\2>K\3\2\2\2?@\7\b\2\2@A\7\t\2\2AB\5\n\6\2BC\5\16\b\2CK\3\2\2\2DE\7\7"+
		"\2\2EF\7\t\2\2FG\5\n\6\2GH\5\22\n\2HI\5\16\b\2IK\3\2\2\2J\64\3\2\2\2J"+
		"9\3\2\2\2J?\3\2\2\2JD\3\2\2\2K\t\3\2\2\2LM\7\32\2\2MR\5\f\7\2NO\7\35\2"+
		"\2OQ\5\f\7\2PN\3\2\2\2QT\3\2\2\2RP\3\2\2\2RS\3\2\2\2S\13\3\2\2\2TR\3\2"+
		"\2\2UV\7$\2\2VW\7\23\2\2Wk\5\32\16\2XY\7$\2\2YZ\7\24\2\2Zk\5\32\16\2["+
		"\\\7$\2\2\\]\7\25\2\2]k\5\32\16\2^_\7$\2\2_`\7\26\2\2`k\5\32\16\2ab\7"+
		"$\2\2bc\7\16\2\2cd\7\36\2\2de\7%\2\2ef\7\34\2\2fg\7%\2\2gh\7\34\2\2hi"+
		"\7\"\2\2ik\7\37\2\2jU\3\2\2\2jX\3\2\2\2j[\3\2\2\2j^\3\2\2\2ja\3\2\2\2"+
		"k\r\3\2\2\2lm\7\13\2\2mn\7\"\2\2no\7\f\2\2op\7\"\2\2p\17\3\2\2\2qr\7\17"+
		"\2\2rs\7\20\2\2st\7\r\2\2tu\7%\2\2u\21\3\2\2\2vw\7\n\2\2w|\5\24\13\2x"+
		"y\7\35\2\2y{\5\24\13\2zx\3\2\2\2{~\3\2\2\2|z\3\2\2\2|}\3\2\2\2}\23\3\2"+
		"\2\2~|\3\2\2\2\177\u0080\7$\2\2\u0080\u0081\7\23\2\2\u0081\u0096\5\32"+
		"\16\2\u0082\u0083\7$\2\2\u0083\u0084\7\23\2\2\u0084\u0096\5\26\f\2\u0085"+
		"\u0086\7$\2\2\u0086\u0087\7\30\2\2\u0087\u0096\7%\2\2\u0088\u0089\7$\2"+
		"\2\u0089\u008a\7\31\2\2\u008a\u0096\7%\2\2\u008b\u008c\7$\2\2\u008c\u008d"+
		"\7\30\2\2\u008d\u008f\5\26\f\2\u008e\u0090\5\20\t\2\u008f\u008e\3\2\2"+
		"\2\u008f\u0090\3\2\2\2\u0090\u0096\3\2\2\2\u0091\u0092\5\30\r\2\u0092"+
		"\u0093\7\30\2\2\u0093\u0094\7%\2\2\u0094\u0096\3\2\2\2\u0095\177\3\2\2"+
		"\2\u0095\u0082\3\2\2\2\u0095\u0085\3\2\2\2\u0095\u0088\3\2\2\2\u0095\u008b"+
		"\3\2\2\2\u0095\u0091\3\2\2\2\u0096\25\3\2\2\2\u0097\u0098\7\36\2\2\u0098"+
		"\u0099\7%\2\2\u0099\u009a\7 \2\2\u009a\u009b\7%\2\2\u009b\u009c\7\34\2"+
		"\2\u009c\u009d\7%\2\2\u009d\u009e\7\37\2\2\u009e\27\3\2\2\2\u009f\u00a0"+
		"\7\22\2\2\u00a0\u00a1\7\36\2\2\u00a1\u00a2\7$\2\2\u00a2\u00a3\7\34\2\2"+
		"\u00a3\u00a4\7\"\2\2\u00a4\u00a5\7\34\2\2\u00a5\u00a6\7\"\2\2\u00a6\u00a7"+
		"\7\37\2\2\u00a7\31\3\2\2\2\u00a8\u00ae\7$\2\2\u00a9\u00ae\7!\2\2\u00aa"+
		"\u00ae\7\"\2\2\u00ab\u00ae\7%\2\2\u00ac\u00ae\7#\2\2\u00ad\u00a8\3\2\2"+
		"\2\u00ad\u00a9\3\2\2\2\u00ad\u00aa\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ac"+
		"\3\2\2\2\u00ae\33\3\2\2\2\n\61JRj|\u008f\u0095\u00ad";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}