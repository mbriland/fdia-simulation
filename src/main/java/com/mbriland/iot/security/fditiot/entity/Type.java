package com.mbriland.iot.security.fditiot.entity;

public enum Type {

	INTEGER("ENTIER", "Entier"),
	FLOAT("FLOAT", "Float"),
	STRING("STRING","String"),
	DEFAULT("DEFAULT", "default");

	private String code;
	private String text;

	private Type(String code, String text) {
		this.code = code;
		this.text = text;
	}

	public String getCode() {
		return code;
	}

	public String getText() {
		return text;
	}

	public static Type getByCode(String typeCode) {
		for (Type t : Type.values()) {
			if (t.code.equals(typeCode)) {
				return t;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.text;
	}

}