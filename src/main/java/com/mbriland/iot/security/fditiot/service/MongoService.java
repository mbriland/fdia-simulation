package com.mbriland.iot.security.fditiot.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

@Service
public class MongoService {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	public boolean isCollectionExist(String collectionName) {
		return mongoTemplate.collectionExists(collectionName);
	}
	
	public void dropCollection(String collection) {
		mongoTemplate.dropCollection(collection);
	}
	
	public Set<String> getCollectionNames() {
		return mongoTemplate.getCollectionNames();
	}
	

}
