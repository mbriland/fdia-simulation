package com.mbriland.iot.security.fditiot.entity;

import java.util.regex.Pattern;

public enum DataType {

    STRING("([A-Za-z_$][A-Za-z0-9_$*]*)|(^([\"]+.*[\"]+)$)|[0-9]*\\/[0-9]*\\/[0-9]*") {
        @Override
        public Object getValue(String input) {
            return input;
        }
    },

    NUMBER("^-?[0-9]\\d*(\\.\\d+)?$") {
        @Override
        public Object getValue(String input) {
            try {
                return Double.parseDouble(input);
            }
            catch (NumberFormatException e) {
                return 0;
            }
        }
    };

    private Pattern pattern;
    private DataType(String pattern) { 
        this.pattern = Pattern.compile(pattern);
    }

    public boolean isType(String input){
        return pattern.matcher(input).matches();
    }

    public abstract Object getValue(String input);

    public static DataType getType(String input){
        for(DataType type : values())
            if (type.isType(input))
                return type;
        return STRING;
    }
}