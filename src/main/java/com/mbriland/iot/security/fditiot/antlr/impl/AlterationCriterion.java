package com.mbriland.iot.security.fditiot.antlr.impl;

public class AlterationCriterion {

	private Object ident;
	private Op  op;
	private Object expr;
	
	private AttenuationCriterion attenuationCriterion;
	
	public AlterationCriterion(Object ident, Op op, Object expr) {
		super();
		this.ident = ident;
		this.op = op;
		this.expr = expr;
	}
	public AlterationCriterion() {
		// TODO Auto-generated constructor stub
	}
	public Object getIdent() {
		return ident;
	}
	public void setIdent(Object ident) {
		this.ident = ident;
	}
	public Op getOp() {
		return op;
	}
	public void setOp(Op op) {
		this.op = op;
	}
	public Object getExpr() {
		return expr;
	}
	public void setExpr(Object expr) {
		this.expr = expr;
	}
	public AttenuationCriterion getAttenuationCriterion() {
		return attenuationCriterion;
	}
	public void setAttenuationCriterion(AttenuationCriterion attenuationCriterion) {
		this.attenuationCriterion = attenuationCriterion;
	}
	
	
	
	
	
}
