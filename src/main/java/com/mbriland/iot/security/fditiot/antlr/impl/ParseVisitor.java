package com.mbriland.iot.security.fditiot.antlr.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.omg.CORBA.CTX_RESTRICT_SCOPE;

import com.mbriland.iot.security.fditiot.antlr.fd2iotBaseVisitor;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.AffectationContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.AlterationCriteriaContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.AlterationCriterionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.ArrayIncrementationContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.AttenuationCriteriaContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.DecimalLitealTypeContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.DifferentSelectionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.EqualSelectionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.EvolContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.EvolutionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.EvolutionIncrementationContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.ExecutionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.ExecutionListContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.GreaterThanSelectionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.IdentTypeContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.IsInsideCircleSelectionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.LessThanSelectionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.MultiplyOffsetContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.OffsetContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.RootContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.ScenarioDeclarationContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.SelectionCriteriaContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.SelectionCriterionContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.StringLiteralTypeContext;
import com.mbriland.iot.security.fditiot.antlr.fd2iotParser.TimeframeContext;

public class ParseVisitor extends fd2iotBaseVisitor<Scenario> {

	Stack<Object> stack = new Stack<>();

	@Override
	public Scenario visitRoot(RootContext ctx) {
		System.out.println("START");

		Scenario scenario = new Scenario();

		List<Execution> executions = new ArrayList<>();
		scenario.setExecs(executions);
		stack.push(scenario); // stack (scenario);

		// visite du debut de la grammaire(nom scenario, ticker)
		this.visit(ctx.scenarioDeclaration());

		// visite de la liste des excecution déclaré
		this.visit(ctx.executionList());

		System.out.println(stack);
		return (Scenario) stack.pop();
	}

	@Override
	public Scenario visitScenarioDeclaration(ScenarioDeclarationContext ctx) {

		Scenario scenario = (Scenario) stack.pop();
		scenario.setName(ctx.STRING_LITERAL().getText());
		scenario.setTicker(Integer.parseInt(ctx.DECIMAL_LITERAL().getText()));
		scenario.setGeoLocation(ctx.REAL_LITERAL(0).getText()+','+ctx.REAL_LITERAL(1).getText());
		stack.push(scenario);

		return null;
	}

	@Override
	public Scenario visitExecutionList(ExecutionListContext ctx) {

		for (ExecutionContext prim : ctx.execution()) {
			this.visit(prim);
			// System.out.println(prim.getText());
		}

		return null;
	}

	@Override
	public Scenario visitExecution(ExecutionContext ctx) {
		Execution execution;
		Scenario scenario;
		switch (ctx.getChild(0).getText()) {
		case "create":
			System.out.println("create");
			execution = new Execution();
			execution.setPrimitive(EPrimitive.CREATE);

			this.visit(ctx.alterationCriteria()); // stack(alterationlist,scenario);

			execution.setAlterationCriterion((ArrayList<AlterationCriterion>) stack.pop());// stack(scenario)

			this.visit(ctx.timeframe()); // stack(time,scenario);
			execution.setTimeframe((TimeFrame) stack.pop());// stack(scenario);
			scenario = (Scenario) stack.pop();// stack empty
			System.out.println("noramelment vide : " + stack);
			scenario.getExecs().add(execution);
			stack.push(scenario);// stack(scenario);
			break;

		case "alter":
			execution = new Execution();
			execution.setPrimitive(EPrimitive.ALTER);

			this.visit(ctx.selectionCriteria());
			execution.setSelectionCriterion((List<SelectionCriterion>) stack.pop());

			this.visit(ctx.alterationCriteria()); // stack(alterationlist,scenario);

			execution.setAlterationCriterion((ArrayList<AlterationCriterion>) stack.pop());// stack(scenario)

			this.visit(ctx.timeframe()); // stack(time,scenario);
			execution.setTimeframe((TimeFrame) stack.pop());// stack(scenario);
			scenario = (Scenario) stack.pop();// stack empty
			System.out.println("noramelment vide : " + stack);
			scenario.getExecs().add(execution);
			stack.push(scenario);// stack(scenario);
			break;

		case "delete":
			execution = new Execution();
			execution.setPrimitive(EPrimitive.DELETE);

			this.visit(ctx.selectionCriteria());
			execution.setSelectionCriterion((List<SelectionCriterion>) stack.pop());

			this.visit(ctx.timeframe());
			execution.setTimeframe((TimeFrame) stack.pop());// stack(scenario);

			scenario = (Scenario) stack.pop();// stack empty
			System.out.println("noramelment vide : " + stack);
			scenario.getExecs().add(execution);
			stack.push(scenario);// stack(scenario);
			break;

		case "copy":
			execution = new Execution();
			execution.setPrimitive(EPrimitive.COPY);

			this.visit(ctx.selectionCriteria());
			execution.setSelectionCriterion((List<SelectionCriterion>) stack.pop());

			this.visit(ctx.alterationCriteria()); // stack(alterationlist,scenario);

			execution.setAlterationCriterion((ArrayList<AlterationCriterion>) stack.pop());// stack(scenario)

			this.visit(ctx.timeframe()); // stack(time,scenario);
			execution.setTimeframe((TimeFrame) stack.pop());// stack(scenario);
			scenario = (Scenario) stack.pop();// stack empty
			System.out.println("noramelment vide : " + stack);
			scenario.getExecs().add(execution);
			stack.push(scenario);// stack(scenario);
			break;

		default:
			System.out.print("Aucune primitive correct");
			break;
		}
		return null;
	}

	@Override
	public Scenario visitAlterationCriteria(AlterationCriteriaContext ctx) {
		System.out.println("NOEUD VISIT ALTERATION CRITERIA");
		List<AlterationCriterion> alterationCriterion = new ArrayList<>();
		stack.push(alterationCriterion);
		for (AlterationCriterionContext alte : ctx.alterationCriterion()) {
			this.visit(alte); // VISITE les differentes alteration e.g. Affectation evolution offset
		}
		return null;
	}

	@Override
	public Scenario visitAffectation(AffectationContext ctx) {

		AlterationCriterion alterationCriterion = new AlterationCriterion();
		alterationCriterion.setIdent(ctx.ID().getText());
		this.visit(ctx.type());

		alterationCriterion.setExpr(stack.pop());
		alterationCriterion.setOp(Op.EQUAL);

		System.out.println(stack);

		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterion);
		stack.push(listAlteration);

		return null;
	}

	@Override
	public Scenario visitEvolution(EvolutionContext ctx) {

		AlterationCriterion alterationCriterion = new AlterationCriterion();
		alterationCriterion.setIdent(ctx.ID().getText());
		this.visit(ctx.evol()); // oncherche a recupérer l'objet evol
		alterationCriterion.setExpr(stack.pop());
		alterationCriterion.setOp(Op.EQUAL);

		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterion);
		stack.push(listAlteration);

		return null;
	}

	@Override
	public Scenario visitEvol(EvolContext ctx) {

		EvolutionCriterion evolutionCriterion = new EvolutionCriterion(
				Double.parseDouble(ctx.REAL_LITERAL(0).getText()),
				Double.parseDouble(ctx.REAL_LITERAL(1).getText()),
				Double.parseDouble(ctx.REAL_LITERAL(2).getText()));

		stack.push(evolutionCriterion);
		return null;
	}

	@Override
	public Scenario visitOffset(OffsetContext ctx) {

		AlterationCriterion alterationCriterion = new AlterationCriterion();
		alterationCriterion.setIdent(ctx.ID().getText());

		alterationCriterion.setExpr(ctx.REAL_LITERAL().getText());
		alterationCriterion.setOp(Op.INCREMENT);

		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterion);
		stack.push(listAlteration);

		return null;
	}

	@Override
	public Scenario visitMultiplyOffset(MultiplyOffsetContext ctx) {

		AlterationCriterion alterationCriterion = new AlterationCriterion();
		alterationCriterion.setIdent(ctx.ID().getText());

		alterationCriterion.setExpr(ctx.REAL_LITERAL().getText());
		alterationCriterion.setOp(Op.MULTIPLY);

		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterion);
		stack.push(listAlteration);

		return null;
	}
	
	@Override
	public Scenario visitEvolutionIncrementation(EvolutionIncrementationContext ctx) {
		

		AlterationCriterion alterationCriterion = new AlterationCriterion();
		alterationCriterion.setIdent(ctx.ID().getText());
		this.visit(ctx.evol()); // oncherche a recupérer l'objet evol
		alterationCriterion.setExpr(stack.pop());
		alterationCriterion.setOp(Op.INCREMENT);
		
		
		if(ctx.attenuationCriteria() != null) {
			visit(ctx.attenuationCriteria());
			
			AttenuationCriterion attenuationCriterion =  (AttenuationCriterion) stack.pop();
			attenuationCriterion.setIdent(alterationCriterion.getIdent());
			alterationCriterion.setAttenuationCriterion(attenuationCriterion);
		}else {
			System.out.println("Aucune attéunation de défini");
		}
		

		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterion);
		stack.push(listAlteration);

		return null;
		
	}
	
	@Override
	public Scenario visitArrayIncrementation(ArrayIncrementationContext ctx) {
		

		AlterationCriterionArray alterationCriterionArray = new AlterationCriterionArray();
		alterationCriterionArray.setIdent(ctx.array().ID().getText());

		alterationCriterionArray.setExpr(ctx.REAL_LITERAL().getText());
		alterationCriterionArray.setOp(Op.INCREMENT);
		
		alterationCriterionArray.setStart(Integer.parseInt(ctx.array().DECIMAL_LITERAL(0).getText()));
		alterationCriterionArray.setEnd(Integer.parseInt(ctx.array().DECIMAL_LITERAL(1).getText()));



		ArrayList<AlterationCriterion> listAlteration = (ArrayList<AlterationCriterion>) stack.pop();
		listAlteration.add(alterationCriterionArray);
		stack.push(listAlteration);

		return null;
		
	}

	@Override
	public Scenario visitIdentType(IdentTypeContext ctx) {
		stack.push(ctx.ID().getText());
		return null;
	}

	@Override
	public Scenario visitDecimalLitealType(DecimalLitealTypeContext ctx) {
		stack.push(Integer.parseInt(ctx.DECIMAL_LITERAL().getText()));
		return null;
	}

	@Override
	public Scenario visitStringLiteralType(StringLiteralTypeContext ctx) {
		stack.push(ctx.STRING_LITERAL().getText().replaceAll("^\"|\"$", ""));

		// System.out.println(ctx.STRING_LITERAL().getText().replaceAll("^\"|\"$", ""));
		return null;
	}

	@Override
	public Scenario visitTimeframe(TimeframeContext ctx) {

		TimeFrame time = new TimeFrame(Integer.parseInt(ctx.DECIMAL_LITERAL(0).getText()),
				Integer.parseInt(ctx.DECIMAL_LITERAL(1).getText()));
		stack.push(time);

		// System.out.println(ctx.INT().get(0));
		// System.out.println(ctx.INT().get(1));

		return null;
	}

	@Override
	public Scenario visitSelectionCriteria(SelectionCriteriaContext ctx) {
		List<SelectionCriterion> selectionCriterion = new ArrayList<>();
		stack.push(selectionCriterion);
		for (SelectionCriterionContext select : ctx.selectionCriterion()) {
			this.visit(select);
		}
		return null;
	}

	@Override
	public Scenario visitEqualSelection(EqualSelectionContext ctx) {

		SelectionCriterion selection = new SelectionCriterion();
		selection.setIdent(ctx.ID().getText());
		selection.setOp(Op.EQUAL);
		visit(ctx.type());
		selection.setExpr(stack.pop());

		ArrayList<SelectionCriterion> listSelection = (ArrayList<SelectionCriterion>) stack.pop();
		listSelection.add(selection);

		stack.push(listSelection);

		return null;
	}

	@Override
	public Scenario visitGreaterThanSelection(GreaterThanSelectionContext ctx) {

		SelectionCriterion selection = new SelectionCriterion();
		selection.setIdent(ctx.ID().getText());
		selection.setOp(Op.GREATERTHAN);
		visit(ctx.type());
		selection.setExpr(stack.pop());

		ArrayList<SelectionCriterion> listSelection = (ArrayList<SelectionCriterion>) stack.pop();
		listSelection.add(selection);

		stack.push(listSelection);

		return null;
	}

	@Override
	public Scenario visitLessThanSelection(LessThanSelectionContext ctx) {

		SelectionCriterion selection = new SelectionCriterion();
		selection.setIdent(ctx.ID().getText());
		selection.setOp(Op.LESSERTHAN);
		visit(ctx.type());
		selection.setExpr(stack.pop());

		ArrayList<SelectionCriterion> listSelection = (ArrayList<SelectionCriterion>) stack.pop();
		listSelection.add(selection);

		stack.push(listSelection);

		return null;
	}

	@Override
	public Scenario visitDifferentSelection(DifferentSelectionContext ctx) {

		SelectionCriterion selection = new SelectionCriterion();
		selection.setIdent(ctx.ID().getText());
		selection.setOp(Op.DIFFERENT);
		visit(ctx.type());
		selection.setExpr(stack.pop());

		ArrayList<SelectionCriterion> listSelection = (ArrayList<SelectionCriterion>) stack.pop();
		listSelection.add(selection);

		stack.push(listSelection);

		return null;
	}

	@Override
	public Scenario visitIsInsideCircleSelection(IsInsideCircleSelectionContext ctx) {

		SelectionCriterion selection = new SelectionCriterion();
		selection.setIdent(ctx.ID().getText());
		selection.setOp(Op.ISINSIDECIRCLE);

		Double x = Double.parseDouble(ctx.REAL_LITERAL(0).getText());
		Double y = Double.parseDouble(ctx.REAL_LITERAL(1).getText());
		Double l = Double.parseDouble(ctx.DECIMAL_LITERAL().getText());

		Circle c = new Circle(x, y, l);

		selection.setExpr(c);
		
		ArrayList<SelectionCriterion> listSelection = (ArrayList<SelectionCriterion>) stack.pop();
		listSelection.add(selection);

		stack.push(listSelection);

		return null;
	}

	@Override
	public Scenario visitAttenuationCriteria(AttenuationCriteriaContext ctx) {
		AttenuationCriterion attenuation = new AttenuationCriterion();
		attenuation.setOp(Op.EQUAL);
		attenuation.setExpr(Double.parseDouble(ctx.REAL_LITERAL().getText()));
		
		stack.push(attenuation);
		
		return null;
	}

}
