package com.mbriland.iot.security.fditiot.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import com.mbriland.iot.security.fditiot.Main;
import com.mbriland.iot.security.fditiot.config.StageManager;
import com.mbriland.iot.security.fditiot.entity.Category;
import com.mbriland.iot.security.fditiot.entity.ConfigurationRecord;
import com.mbriland.iot.security.fditiot.entity.JsonImportedWithProperty;
import com.mbriland.iot.security.fditiot.entity.Type;
import com.mbriland.iot.security.fditiot.service.ConfigurationService;
import com.mbriland.iot.security.fditiot.service.MongoService;
import com.mbriland.iot.security.fditiot.service.RecordService;
import com.mbriland.iot.security.fditiot.view.FxmlView;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

@Controller
public class MappingJsonController {

	@Lazy
	@Autowired
	StageManager stageManager;

	private Main app;
	
	JsonImportedWithProperty JsonImportedWithProperty;

	@FXML
	private TextField recordName;

	@FXML
	private TableView<ConfigurationRecord> propertyTable;

	@FXML
	private TableColumn<ConfigurationRecord, String> columnProperty;

	@FXML
	private TableColumn<ConfigurationRecord, Category> columnCategory;

	@FXML
	private TableColumn<ConfigurationRecord, Type> columnType;

	@FXML
	private Button bouttonOk;

	@Autowired
	ConfigurationService configurationService;
	
	@Autowired
	RecordService recordService;
	
	@Autowired
	MongoService mongoService;

	public void initData(JsonImportedWithProperty jsonImportedWithProperty) {

		this.JsonImportedWithProperty = jsonImportedWithProperty;
		ArrayList<ConfigurationRecord> configurationRecords = new ArrayList<ConfigurationRecord>();

		HashSet<String> hashSet = jsonImportedWithProperty.getPropertyHashSet();
		for (String nameProperty : hashSet) {
			ConfigurationRecord configurationRecord = new ConfigurationRecord();
			configurationRecord.setProperty(nameProperty);
			configurationRecord.setCategory(Category.DEFAULT.getCode());
			configurationRecord.setType(Type.DEFAULT.getCode());
			configurationRecords.add(configurationRecord);
		}

		columnProperty.setCellValueFactory(new PropertyValueFactory<ConfigurationRecord, String>("property"));
		ObservableList<ConfigurationRecord> list = FXCollections.observableArrayList(configurationRecords);
		propertyTable.setItems(list);
		propertyTable.setEditable(true);

		// ======COMBOBOX COLONNE CATEGORY

		ObservableList<Category> categoryList = FXCollections.observableArrayList(Category.values());

		columnCategory.setCellValueFactory(
				new Callback<CellDataFeatures<ConfigurationRecord, Category>, ObservableValue<Category>>() {

					@Override
					public ObservableValue<Category> call(CellDataFeatures<ConfigurationRecord, Category> param) {
						ConfigurationRecord configurationRecord = param.getValue();

						String categoryCode = configurationRecord.getCategory();
						Category category = Category.getByCode(categoryCode);
						return new SimpleObjectProperty<Category>(category);
					}
				});

		columnCategory.setCellFactory(ComboBoxTableCell.forTableColumn(categoryList));

		columnCategory.setOnEditCommit((CellEditEvent<ConfigurationRecord, Category> event) -> {
			TablePosition<ConfigurationRecord, Category> pos = event.getTablePosition();

			Category newCategory = event.getNewValue();

			int row = pos.getRow();
			ConfigurationRecord configurationRecord = event.getTableView().getItems().get(row);

			configurationRecord.setCategory(newCategory.getCode());
		});

		columnCategory.setEditable(true);

		// ======COMBOBOX COLONNE TYPE

		ObservableList<Type> typeList = FXCollections.observableArrayList(Type.values());

		columnType.setCellValueFactory(
				new Callback<CellDataFeatures<ConfigurationRecord, Type>, ObservableValue<Type>>() {

					@Override
					public SimpleObjectProperty<Type> call(CellDataFeatures<ConfigurationRecord, Type> param) {
						ConfigurationRecord configurationRecord = param.getValue();

						String typeCode = configurationRecord.getType();
						Type type = Type.getByCode(typeCode);
						return new SimpleObjectProperty<Type>(type);
					}
				});

		columnType.setCellFactory(ComboBoxTableCell.forTableColumn(typeList));

		columnType.setOnEditCommit((CellEditEvent<ConfigurationRecord, Type> event) -> {
			TablePosition<ConfigurationRecord, Type> pos = event.getTablePosition();

			Type newType = event.getNewValue();

			int row = pos.getRow();
			ConfigurationRecord configurationRecord = event.getTableView().getItems().get(row);

			configurationRecord.setType(newType.getCode());
		});

		columnType.setEditable(true);

		propertyTable.setEditable(true);
	}

	public void onClickOk(ActionEvent actionEvent) throws IOException {
		
		if (recordName.getText().trim().isEmpty()) {

			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Le nom de l'enregistrement ne peut étre vide");
			alert.showAndWait();
		} else if(mongoService.isCollectionExist(recordName.getText())){
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Erreur");
			alert.setHeaderText("Le nom de l'enregistrement est déja utilisé");
			alert.showAndWait();
		}else {

			List<ConfigurationRecord> list = propertyTable.getItems();
			
			configurationService.insertAllConfigInCollection( list, recordName.getText()+"_config");
			
			recordService.insertAll(JsonImportedWithProperty.getRecord(), recordName.getText());
			
			stageManager.switchScene(FxmlView.ROOT);

		}
	}

	public Main getApp() {
		return app;
	}

	public void setApp(Main app) {
		this.app = app;
	}

}
